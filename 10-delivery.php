<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Ticket delivery options</h1>
        <p>Munich Oktoberfest</p>
    </div>

    <div class="avaiable-event">
        <div class="uk-grid uk-grid-collapse uk-flex-middle">
            <div class="uk-width-1-2">
                <h4 class="event truncate">Delivery of your tickets</h4>
            </div>
            <div class="uk-width-1-2 uk-text-right">
                Prices: <span class="currency">£ GBP</span>
            </div>
        </div>
    </div>


    <div class="list-delivery">
        <div class="ticket-avai">
            <div class="uk-grid uk-grid-divider">
                <div class="uk-width-2-3">
                    <div class="uk-grid ">
                        <div class="uk-width-1-1">
                            <div class="info">
                                <h4>Munich Oktoberfest Meet & Greet Ticket Collection Service</h4>
                                <span>More information</span> <img src="img/confirm-ic.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-3 uk-flex uk-flex-middle uk-flex-center nth-2-divider">
                    <div class="act">
                        <span>Free (£0)</span>
                        <a href="#" class="uk-button uk-button-primary btn-book-now">Select <i class="uk-icon-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="ticket-avai">
            <div class="uk-grid uk-grid-divider">
                <div class="uk-width-2-3">
                    <div class="uk-grid ">
                        <div class="uk-width-1-1">
                            <div class="info">
                                <h4>Hotel delivery in Munich</h4>
                                <span>More information</span> <img src="img/confirm-ic.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-3 uk-flex uk-flex-middle uk-flex-center nth-2-divider">
                    <div class="act">
                        <span>Free (£0)</span>
                        <a href="#" class="uk-button uk-button-primary btn-book-now">Select <i class="uk-icon-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="ticket-avai">
            <div class="uk-grid uk-grid-divider">
                <div class="uk-width-2-3">
                    <div class="uk-grid ">
                        <div class="uk-width-1-1">
                            <div class="info">
                                <h4>Overseas delivery within the United Kingdom</h4>
                                <span>More information</span> <img src="img/confirm-ic.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-3 uk-flex uk-flex-middle uk-flex-center nth-2-divider">
                    <div class="act">
                        <span>Free (£0)</span>
                        <a href="#" class="uk-button uk-button-primary btn-book-now">Select <i class="uk-icon-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="other-list-review">
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/10-delivery/10-homephone-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Emergency out of hours telephone</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/10-delivery/10-ques-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">I haven't booked my hotel yet?</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/10-delivery/10-ques-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Want to book a hotel at a later date?</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>

<script type="text/javascript">
    $(document).ready(function(){
        $(".score").raty({
            number: 5,
            numberMax: 5,
            starOn: 'img/8a-hotel-search/8a-star-on.png',
            starOff: 'img/8a-hotel-search/8a-star-off.png',
            readOnly: true,
            score: function(){
                return $(this).attr('data-score')
            }
        });
    });
</script>
