<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro uk-margin-bottom-remove">
        <h1>Secure checkout</h1>
        <a href="#" class="required-signin">
            <img src="img/11a-secure-checkout/11a-user-icon.png" alt="">
            Sign in <span>(optional)</span>
        </a>
    </div>
    
    <div class="tip-block">
        <span>Who’s travelling?</span>
        <img src="img/confirm-ic.png" alt="">
    </div>

    <form action="" class="uk-form">
        <div class="guest-item">
            <h4 class="title">Lead Guest</h4>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-2-10 uk-width-1-2">
                    <select name="" id="" class="uk-width-1-1">
                        <option value="">Mr</option>
                        <option value="">Mrs</option>
                    </select>
                </div>
                <div class="uk-width-small-4-10 uk-width-1-2">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="First name">
                </div>
                <div class="uk-width-small-4-10">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Surname/ family name">
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-1-3">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Date of birth">
                </div>
                <div class="uk-width-small-1-3">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Passport number">
                </div>
                <div class="uk-width-small-1-3">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Nationality">
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-5-10">
                    <select name="" id="" class="uk-width-small-8-10 uk-width-1-1">
                        <option value="">United Kingdom +44</option>
                        <option value="">United Kingdom +44</option>
                    </select>
                </div>
                <div class="uk-width-small-5-10">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Cell/mobile telephone number">
                </div>
            </div>
            <div class="uk-grid"data-uk-grid-margin>
                <div class="uk-width-small-5-10">
                    <select name="" id="" class="uk-width-1-1">
                        <option value="">Attending event</option>
                        <option value="">Attending event</option>
                    </select>
                </div>
                <div class="uk-width-small-5-10">
                    <select name="" id="" class="uk-width-1-1">
                        <option value="">Allocate room type</option>
                        <option value="">Allocate room type</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="guest-item">
            <h4 class="title">Guest 2</h4>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-2-10 uk-width-1-2">
                    <select name="" id="" class="uk-width-1-1">
                        <option value="">Mr</option>
                        <option value="">Mrs</option>
                    </select>
                </div>
                <div class="uk-width-small-4-10 uk-width-1-2">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="First name">
                </div>
                <div class="uk-width-small-4-10">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Surname/ family name">
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-1-3">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Date of birth">
                </div>
                <div class="uk-width-small-1-3">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Passport number">
                </div>
                <div class="uk-width-small-1-3">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Nationality">
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-5-10">
                    <select name="" id="" class="uk-width-1-1">
                        <option value="">Attending event</option>
                        <option value="">Attending event</option>
                    </select>
                </div>
                <div class="uk-width-small-5-10">
                    <select name="" id="" class="uk-width-1-1">
                        <option value="">Allocate room type</option>
                        <option value="">Allocate room type</option>
                    </select>
                </div>
            </div>
        </div>
    </form>

    <div class="tip-block">
        <span>Delivery of event tickets</span>
        <img src="img/confirm-ic.png" alt="">
    </div>

    <form action="" class="uk-form">
        <div class="guest-item">
            <h4 class="title">Barcelona hotel delivery</h4>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-1-1">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Hotel name">
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-1-2">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Address line 1">
                </div>
                <div class="uk-width-small-1-2">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Address line 2 (optional)">
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-1-3">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Town/ city">
                </div>
                <div class="uk-width-small-1-3">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Post/ zip code">
                </div>
                <div class="uk-width-small-1-3">
                    <select name="" id="" class="uk-width-1-1">
                        <option value="">Country</option>
                        <option value="">Country 1</option>
                    </select>
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-1-2">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Room(s) booked under name">
                </div>
                <div class="uk-width-small-1-2">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Arrival date (dd/mm/yyyy)">
                </div>
            </div>
        </div>
    </form>
    
    <div class="block-box">
        <h4>What if I don’t have my delivery address yet?</h4>
        <p>You may email your delivery address no later than 14 days prior to the event. Please enter TBC above and your expected arrival date.</p>
    </div>

    <div class="block-box">
        <h4>Stadium ticket collection</h4>
        <ul class="uk-list">
            <li>Museum Reception Desk,  Atletico Madrid Museum,</li>
            <li>Sport Arena Restaurant, next to Gate 23,  Vicente Calderon Stadium,</li>
            <li>Paseo De Los Melancolicos 67, Madrid 28005, Spain</li>
        </ul>
    </div>

    <div class="block-list-condition">
        <form action="" class="uk-form">
            <ul class="uk-list list-condition">
                <li>
                    <h4>Event & ticket conditions</h4>
                </li>
                <li>
                    <label>
                        <input type="checkbox" name="" id="">
                        I have read, understand & agree to the <a href="#">event & ticket conditions</a>
                    </label>
                </li>
                <li>
                    <h4>General Terms & Conditions</h4>
                </li>
                <li>
                    <label>
                        <input type="checkbox" name="" id="">
                        I have read, understand & agree to the <a href="#">general terms & conditions</a>
                    </label>
                </li>
                <li>
                    <h4>Hotel policies & cancellation conditions</h4>
                </li>
                <li>
                    <label>
                        <input type="checkbox" name="" id="">
                        I have read, understand & agree to the <a href="#">hotel policies & conditions</a>
                    </label>
                </li>
                <li>
                    <h4>Extra fees & taxes payable by guests</h4>
                </li>
                <li>
                    <label>
                        <input type="checkbox" name="" id="">
                        I have read, understand & agree to the <a href="#">extra fees & taxes</a>
                    </label>
                </li>
            </ul>
        </form>
    </div>

    <div class="tip-block">
        <h4>Your details</h4>
    </div>

    <div class="uk-form">
        <div class="guest-item">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-1-1">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Email address">
                </div>
                <div class="uk-width-1-1">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Password">
                </div>
                <div class="uk-width-1-1">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Confirm password">
                </div>
            </div>
            <div class="uk-grid uk-grid-small uk-flex-middle" data-uk-grid-margin>
                <div class="uk-width-small-1-2">
                    Where did you hear of Tickazilla?
                </div>
                <div class="uk-width-small-1-2">
                    <select name="" id="" class="uk-width-1-1">
                        <option value="">Please select</option>
                    </select>
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-1-1">
                    <label>
                        <input type="checkbox" name="" id="">
                        Subscribe to Tickailla.com newsletter
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="tip-block">
        <span>Billing address</span>
        <img src="img/confirm-ic.png" alt="">
    </div>

    <div class="uk-form">
        <div class="guest-item">
            <div class="uk-grid uk-grid-small uk-flex-middle" data-uk-grid-margin>
                <div class="uk-width-small-1-2">
                    Enter address or select previous address:
                </div>
                <div class="uk-width-small-1-2">
                    <select name="" id="" class="uk-width-1-1">
                        <option value="">Please select</option>
                    </select>
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-1-1">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Credit Card Holders / Payee's Name">
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-1-2">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Address line 1 or company name">
                </div>
                <div class="uk-width-small-1-2">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Address line 2 (optional)">
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-1-2">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Town/ city">
                </div>
                <div class="uk-width-small-1-2">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="County/ locality">
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-1-2">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Postal/ zip code">
                </div>
                <div class="uk-width-small-1-2">
                    <select name="" id="" class="uk-width-1-1">
                        <option value="">Country</option>
                    </select>
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-1-2">
                    <select name="" id="" class="uk-width-1-1">
                        <option value="">United Kingdom</option>
                    </select>
                </div>
                <div class="uk-width-small-1-2">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Landline telephone number">
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-1-2">
                    <select name="" id="" class="uk-width-1-1">
                        <option value="">United Kingdom</option>
                    </select>
                </div>
                <div class="uk-width-small-1-2">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Mobile/ cell telephone number">
                </div>
            </div>
        </div>
    </div>

    <div class="pay-has-price">
        <div class="uk-grid uk-flex-middle">
            <div class="uk-width-small-2-3">
                <span class="how">How would you like to pay?</span>
                <img src="img/confirm-ic.png" alt="">
            </div>
            <div class="uk-width-small-1-3">
                <span class="prices">Price: <b>£ GBP</b></span>
            </div>
        </div>
    </div>
    
    <div class="list-pay">
        <div class="pay-item">
            <div class="uk-grid uk-grid-small uk-flex-middle">
                <div class="uk-width-1-6 uk-text-center">
                    <img src="img/11a-secure-checkout/jcb-pay.png" alt="">
                </div>
                <div class="uk-width-4-6">
                    <div class="info">
                        <h4 class="truncate">JCB credit card</h4>
                        <span>1.95% charge</span>
                    </div>
                </div>
                <div class="uk-width-1-6 uk-text-center">
                    <span class="price">£6.96</span>
                </div>
            </div>
        </div>
        <div class="pay-item">
            <div class="uk-grid uk-grid-small uk-flex-middle">
                <div class="uk-width-1-6 uk-text-center">
                    <img src="img/11a-secure-checkout/maetro-pay.png" alt="">
                </div>
                <div class="uk-width-4-6">
                    <div class="info">
                        <h4 class="truncate">Maestro debit card</h4>
                        <span>1.95% charge</span>
                    </div>
                </div>
                <div class="uk-width-1-6 uk-text-center">
                    <span class="price">£6.96</span>
                </div>
            </div>
        </div>
        <div class="pay-item">
            <div class="uk-grid uk-grid-small uk-flex-middle">
                <div class="uk-width-1-6 uk-text-center">
                    <img src="img/11a-secure-checkout/mastercard-debit-pay.png" alt="">
                </div>
                <div class="uk-width-4-6">
                    <div class="info">
                        <h4 class="truncate">MasterCard debit card</h4>
                        <span>1.95% charge</span>
                    </div>
                </div>
                <div class="uk-width-1-6 uk-text-center">
                    <span class="price">£6.96</span>
                </div>
            </div>
        </div>
        <div class="pay-item">
            <div class="uk-grid uk-grid-small uk-flex-middle">
                <div class="uk-width-1-6 uk-text-center">
                    <img src="img/11a-secure-checkout/mastercard-pay.png" alt="">
                </div>
                <div class="uk-width-4-6">
                    <div class="info">
                        <h4 class="truncate">MasterCard credit card</h4>
                        <span>1.95% charge</span>
                    </div>
                </div>
                <div class="uk-width-1-6 uk-text-center">
                    <span class="price">£6.96</span>
                </div>
            </div>
        </div>
        <div class="pay-item">
            <div class="uk-grid uk-grid-small uk-flex-middle">
                <div class="uk-width-1-6 uk-text-center">
                    <img src="img/11a-secure-checkout/Debit-pay.png" alt="">
                </div>
                <div class="uk-width-4-6">
                    <div class="info">
                        <h4 class="truncate">VISA debit card</h4>
                        <span>1.95% charge</span>
                    </div>
                </div>
                <div class="uk-width-1-6 uk-text-center">
                    <span class="price">£6.96</span>
                </div>
            </div>
        </div>
        <div class="pay-item">
            <div class="uk-grid uk-grid-small uk-flex-middle">
                <div class="uk-width-1-6 uk-text-center">
                    <img src="img/11a-secure-checkout/visa-pay.png" alt="">
                </div>
                <div class="uk-width-4-6">
                    <div class="info">
                        <h4 class="truncate">VISA debit card</h4>
                        <span>1.95% charge</span>
                    </div>
                </div>
                <div class="uk-width-1-6 uk-text-center">
                    <span class="price">£6.96</span>
                </div>
            </div>
        </div>
        <div class="pay-item">
            <div class="uk-grid uk-grid-small uk-flex-middle">
                <div class="uk-width-1-6 uk-text-center">
                    <img src="img/11a-secure-checkout/electron-pay.png" alt="">
                </div>
                <div class="uk-width-4-6">
                    <div class="info">
                        <h4 class="truncate">VISA Electron debit card</h4>
                        <span>1.95% charge</span>
                    </div>
                </div>
                <div class="uk-width-1-6 uk-text-center">
                    <span class="price">£6.96</span>
                </div>
            </div>
        </div>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
