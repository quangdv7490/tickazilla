<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro uk-margin-bottom-remove">
        <h1>Review cart</h1>
    </div>

    <div class="head-page-intro2">
        <h1>Ticket booking summary</h1>
    </div>
    
    <div class="block-ticket-booking">
        <div class="uk-grid uk-grid-small">
            <div class="uk-width-7-10">
                <h4>Borussia Monchengladbach<br>vs.  EA7 Emporio Armani Olimpia Milano</h4>
                <ul class="uk-list uk-margin-top-remove">
                    <li>Palau Municipal d'Esports de Badalona</li>
                    <li>(Pavello Olimpic de Badalona), Badalona, Spain</li>
                </ul>
                <ul class="uk-list">
                    <li>Spanish Primera La Liga BBVA</li>
                    <li>1st Knockout Round - 2nd Leg</li>
                    <li>31 November or 01, 02 or 03 December 2015</li>
                </ul>
                <h4>Category 1A & 3 Euro Snack Voucher<br>20 x Tickets</h4>
                <ul class="uk-list uk-margin-top-remove">
                    <li>Home/ Neutral Section</li>
                    <li>Citizens from away team country not allowed</li>
                </ul>
            </div>
            <div class="uk-width-3-10 uk-flex uk-flex-bottom uk-flex-right">
                <span class="ticket-booking-price">£1,164.00</span>
            </div>
        </div>

        <div class="wrap-total-ticket-cost">
            <div class="uk-grid uk-grid-small">
                <div class="uk-width-7-10">
                    <h4 class="total-ticket-cost">Total Ticket Cost</h4>
                </div>
                <div class="uk-width-3-10 uk-text-right">
                    <span class="ticket-booking-total">£1,164.00</span>
                </div>
            </div>
        </div>
    </div>

    <div class="head-page-intro2">
        <h1>Hotel booking summary</h1>
    </div>
    
    <div class="block-hotel-booking">
        <h4>Andaman Beach Suites (Superor Sea View)</h4>
        <ul class="uk-list uk-margin-top-remove">
            <li>Lake/ sea side of Phuket - Patong</li>
            <li>C/ Nicaragua s/n, Mollet Del Vallas, 8100, United Kingdom</li>
        </ul>

        <ul class="uk-list">
            <li>50 x rooms</li>
            <li>Adults x 8 l Children x 1</li>
            <li>18 December 2015 - 20 December 2015</li>
            <li>2 night stay</li>
            <li>Buffet breakfast (continental)</li>
        </ul>

        <div class="uk-grid uk-grid-small uk-flex-middle" data-uk-grid-margin="">
            <div class="uk-width-1-1">
                <h4>Room type 1: 15 x Twin rooms</h4>
            </div>
            <div class="uk-width-7-10">
                <ul class="uk-list">
                    <li>Friday 18 December 2015</li>
                    <li>Saturday 19 December 2015</li>
                </ul>
            </div>
            <div class="uk-width-3-10 uk-text-right">
                <span class="hotel-booking-price">£1,000.00</span>
                <span class="hotel-booking-price">£1,150.00</span>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle" data-uk-grid-margin="">
            <div class="uk-width-1-1">
                <h4>Room type 2: 15 x Double rooms</h4>
            </div>
            <div class="uk-width-7-10">
                <ul class="uk-list">
                    <li>Friday 18 December 2015</li>
                    <li>Saturday 19 December 2015</li>
                </ul>
            </div>
            <div class="uk-width-3-10 uk-text-right">
                <span class="hotel-booking-price">£1,145.00</span>
                <span class="hotel-booking-price">£1,250.00</span>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle" data-uk-grid-margin="">
            <div class="uk-width-1-1">
                <h4>Room Type 3: 11 x Double rooms (sole use)</h4>
            </div>
            <div class="uk-width-7-10">
                <ul class="uk-list">
                    <li>Friday 18 December 2015</li>
                    <li>Saturday 19 December 2015</li>
                </ul>
            </div>
            <div class="uk-width-3-10 uk-text-right">
                <span class="hotel-booking-price">£1,145.00</span>
                <span class="hotel-booking-price">£1,250.00</span>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle" data-uk-grid-margin="">
            <div class="uk-width-1-1">
                <h4>Room Type 4: 3 x Twin rooms (sole use)</h4>
            </div>
            <div class="uk-width-7-10">
                <ul class="uk-list">
                    <li>Friday 18 December 2015</li>
                    <li>Saturday 19 December 2015</li>
                </ul>
            </div>
            <div class="uk-width-3-10 uk-text-right">
                <span class="hotel-booking-price">£1,145.00</span>
                <span class="hotel-booking-price">£1,250.00</span>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle" data-uk-grid-margin="">
            <div class="uk-width-1-1">
                <h4>Room Type 5: 3 x Single rooms</h4>
            </div>
            <div class="uk-width-7-10">
                <ul class="uk-list">
                    <li>Friday 18 December 2015</li>
                    <li>Saturday 19 December 2015</li>
                </ul>
            </div>
            <div class="uk-width-3-10 uk-text-right">
                <span class="hotel-booking-price">£1,450.00</span>
                <span class="hotel-booking-price">£1,250.00</span>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle" data-uk-grid-margin="">
            <div class="uk-width-1-1">
                <h4>Room type 6: 1 x Triple room</h4>
            </div>
            <div class="uk-width-7-10">
                <ul class="uk-list">
                    <li>Friday 18 December 2015</li>
                    <li>Saturday 19 December 2015</li>
                </ul>
            </div>
            <div class="uk-width-3-10 uk-text-right">
                <span class="hotel-booking-price">£1,250.00</span>
                <span class="hotel-booking-price">£1,450.00</span>
            </div>
        </div>

        <div class="wrap-total-ticket-cost">
            <div class="uk-grid uk-grid-small">
                <div class="uk-width-7-10">
                    <h4 class="total-ticket-cost">Total Hotel Cost</h4>
                </div>
                <div class="uk-width-3-10 uk-text-right">
                    <span class="ticket-booking-total">£14,430.00</span>
                </div>
            </div>
        </div>
    </div>

    <div class="head-page-intro2">
        <h1>Ticket delivery summary</h1>
    </div>

    <div class="block-common-review">
        <div class="uk-grid uk-grid-small uk-flex-middle" data-uk-grid-margin="">
            <div class="uk-width-7-10">
                <h4>Hotel delivery in Barcelona</h4>
            </div>
            <div class="uk-width-3-10 uk-text-right">
                <span class="booking-price">£13.00</span>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle" data-uk-grid-margin="">
            <div class="uk-width-7-10">
                <h4 class="has-cost">Total delivery cost</h4>
            </div>
            <div class="uk-width-3-10 uk-text-right">
                <span class="booking-price">£13.00</span>
            </div>
        </div>
    </div>

    <div class="head-page-intro2">
        <h1>Debit/ credit card summary</h1>
    </div>

    <div class="block-common-review">
        <div class="uk-grid uk-grid-small uk-flex-middle" data-uk-grid-margin="">
            <div class="uk-width-7-10">
                <h4>MasterCard credit card l 1.95% charge</h4>
            </div>
            <div class="uk-width-3-10 uk-text-right">
                <span class="booking-price">£6.96</span>
            </div>
        </div>
    </div>

    <div class="block-common-review">
        <div class="uk-grid uk-grid-small uk-flex-middle" data-uk-grid-margin="">
            <div class="uk-width-7-10">
                <h4 class="has-cost">Sub total cost</h4>
            </div>
            <div class="uk-width-3-10 uk-text-right">
                <span class="booking-price">£363.96</span>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle" data-uk-grid-margin="">
            <div class="uk-width-7-10">
                <h4 class="has-cost">Promotional discounts & vouchers</h4>
            </div>
            <div class="uk-width-3-10 uk-text-right">
                <span class="booking-price">£0.00</span>
            </div>
        </div>
        <div class="uk-grid uk-grid-small" data-uk-grid-margin="">
            <div class="uk-width-7-10">
                <h4 class="has-cost">VAT @ 20%</h4>
                <p>Tickets to events that are performed in the UK are subject to VAT (including delivery charge & credit card fees, if applicable).</p>
            </div>
            <div class="uk-width-3-10 uk-flex uk-flex-top uk-flex-right">
                <span class="booking-price">£0.00</span>
            </div>
        </div>
    </div>

    <div class="head-page-intro3">
        <div class="inner-head">
            <div class="uk-grid uk-flex-middle" data-uk-grid-margin="">
                <div class="uk-width-7-10">
                    <h4 class="total-cost">Total cost</h4>
                </div>
                <div class="uk-width-3-10 uk-text-right">
                    <span class="total-price">£363.96</span>
                </div>
            </div>
        </div>
        <div class="info-cost">
            <div class="uk-text-right">
                <span>Prices quoted in GBP (British Pound Sterling)</span>
            </div>
            <div class="uk-text-center uk-margin">
                <a href="#" class="uk-button uk-button-primary">Payment page <i class="uk-icon-angle-right"></i></a>
            </div>
        </div>
    </div>

    <div class="head-page-intro2">
        <h1>Want to avoid paying the VAT Charge?</h1>
    </div>

    <div class="block-common-review">
        <ul class="uk-list">
            <li>If you book your event ticket together with a hotel in the same transaction then VAT is not chargeable. You will not pay VAT!</li>
            <li>This type of sale falls under the Tour Operators Margin Scheme where UK VAT is not payable.</li>
        </ul>
        <a href="#" class="book-hotel-event">Book a hotel for this event <i class="uk-icon-caret-right"></i></a>
    </div>

    <div class="promotion-discount">
        <div class="inner">
            <h4>Promotional codes & discount vouchers</h4>
            <p>Please enter code and press Apply discount</p>
        </div>
        <div class="inner">
            <div class="uk-grid uk-form">
                <div class="uk-width-1-2 uk-width-small-2-3">
                    <input type="text" name="" id="" class="uk-width-1-1" placeholder="Enter discount code">
                </div>
                <div class="uk-width-1-2 uk-width-small-1-3 uk-text-center">
                    <a href="#" class="uk-button uk-button-primary">Apply discount</a>
                </div>
            </div>
        </div>
        <div class="payment-page">
            <a href="#" class="uk-button uk-button-primary">Payment page  <i class="uk-icon-angle-right"></i></a>
        </div>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
