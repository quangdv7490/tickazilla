<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Customer Support</h1>
    </div>

    <div class="other-list-review">
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/12-customer-support/your-account.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Your account</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/12-customer-support/buble.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Customer reviews</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/12-customer-support/dolla.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Payment help</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/12-customer-support/ques.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Frequently asked questions</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/12-customer-support/clock.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Opening times</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/12-customer-support/phone.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Contact us</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/12-customer-support/obj-1.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Event & ticket conditions</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/12-customer-support/file.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">General terms & conditions</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/12-customer-support/peoples.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">About us</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/12-customer-support/block.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Privacy policy</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/12-customer-support/file2.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Cookie policy</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/12-customer-support/tz.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Tickazilla desktop website</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
