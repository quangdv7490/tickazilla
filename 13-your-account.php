<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Your account</h1>
        <p>Your online Tickazilla customer account</p>
    </div>

    <div class="block-account">
        <div class="head-title">
            <h4 class="uk-flex uk-flex-middle"><img src="img/13-account/13-user-icon.png" alt=""> Sign-in using email</h4>
            <p>Please sign-in using your email address and password</p>
        </div>
        <form action="" class="uk-form uk-form-stacked">
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <div class="uk-grid">
                        <div class="uk-width-7-10">
                            <input type="text" name="" id="" class="uk-width-1-1" placeholder="Email address…">
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <label>
                        <input type="checkbox" name="" id="">
                        Stay signed in
                    </label>
                </div>
            </div>
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <div class="uk-grid">
                        <div class="uk-width-7-10">
                            <input type="password" name="" id="" class="uk-width-1-1" placeholder="Password...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <a href="#" class="uk-button-link">Forgot your password?</a>
                </div>
            </div>
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <div class="uk-text-center">
                        <a href="#" class="uk-button uk-button-primary">Sign in <i class="uk-icon-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="block-account">
        <div class="head-title">
            <h4 class="uk-flex uk-flex-middle"><img src="img/13-account/face-login.png" alt=""> Sign-in using Facebook</h4>
            <p>Please sign-in using your Facebook account</p>
        </div>
        <div class="uk-text-center">
            <div class="uk-button-group">
                <div class="uk-button uk-button-primary"><i class="uk-icon-facebook"></i></div>
                <a href="#" class="uk-button uk-button-primary">Sign in with Facebook</a>
            </div>
        </div>
    </div>

    <div class="block-account">
        <div class="head-title">
            <h4 class="uk-flex uk-flex-middle"><img src="img/13-account/sign-in-plus.png" alt=""> Create an account</h4>
            <p>Register  your personal details and create an account</p>
        </div>
        <form action="" class="uk-form uk-form-stacked">
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <div class="uk-grid">
                        <div class="uk-width-7-10">
                            <input type="text" name="" id="" class="uk-width-1-1" placeholder="E-mail address…">
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <div class="uk-grid">
                        <div class="uk-width-7-10">
                            <input type="text" name="" id="" class="uk-width-1-1" placeholder="Confirm e-mail address…">
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <div class="uk-grid">
                        <div class="uk-width-7-10">
                            <input type="password" name="" id="" class="uk-width-1-1" placeholder="Password...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <div class="uk-grid">
                        <div class="uk-width-7-10">
                            <input type="password" name="" id="" class="uk-width-1-1" placeholder="Confirm password…">
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <div class="uk-grid">
                        <div class="uk-width-7-10">
                            <input type="text" name="" id="" class="uk-width-1-1" placeholder="First name…">
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <div class="uk-grid">
                        <div class="uk-width-7-10">
                            <input type="text" name="" id="" class="uk-width-1-1" placeholder="Last name…">
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-form-row">
                <label>
                    <input type="checkbox" name="" id="">
                    I have read, understand & agree to your website <a href="#">terms & conditions</a>
                </label>
            </div>
            <div class="uk-form-row">
                <label>
                    <input type="checkbox" name="" id="">
                    I have read, understand & agree to your <a href="#">privacy policy</a> and <a href="#">cookie policy</a>
                </label>
            </div>
            <div class="uk-form-row">
                <label>
                    <input type="checkbox" name="" id="">
                    Subscribe to Tickazilla newsletter
                </label>
            </div>
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <div class="uk-text-center">
                        <button type="button" class="uk-button uk-button-primary">Create an account <i class="uk-icon-angle-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
