<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Customer reviews</h1>
        <p>Our customer experiences</p>
    </div>

    <div class="block-customer">
        <div class="recommend">
            <h4 class="uk-flex uk-flex-middle"><img src="img/14-customer-review/check-green.png" alt=""> 97% recommend Tickazilla.com!</h4>
        </div>
        <div class="uk-accordion nav-accord">
            <h4 class="uk-accordion-title"><img src="img/14-customer-review/plan.png" alt=""> Planning a trip?</h4>
            <div class="uk-accordion-content">
                <p>
                    Our customers have written honest reviews about their Tickazilla experience so
                    you can decide which hotels and events are suitable for your holiday.
                </p>
                <p>
                    We trust our customer’s reviews and testimonials will help you plan your holiday
                    better and ensure you have a memorable trip.
                </p>
                <p>
                    You will also find an insight into the reliability of Tickazilla and the level of service
                    to expect from our company.
                </p>
                <p>
                    Don’t forget you can also check independent hotel reviews via the Tripadvisor
                    links we feature on our hotel booking pages.
                </p>
            </div>
            <h4 class="uk-accordion-title"><img src="img/14-customer-review/slime.png" alt=""> Customer satisfaction</h4>
            <div class="uk-accordion-content">
                <p>
                    Our customers have written honest reviews about their Tickazilla experience so
                    you can decide which hotels and events are suitable for your holiday.
                </p>
                <p>
                    We trust our customer’s reviews and testimonials will help you plan your holiday
                    better and ensure you have a memorable trip.
                </p>
                <p>
                    You will also find an insight into the reliability of Tickazilla and the level of service
                    to expect from our company.
                </p>
                <p>
                    Don’t forget you can also check independent hotel reviews via the Tripadvisor
                    links we feature on our hotel booking pages.
                </p>
            </div>
        </div>
    </div>
    
    <div class="list-review">
        <a href="#" class="uk-grid uk-grid-collapse uk-flex-middle review-item">
            <div class="uk-width-small-1-10 uk-width-2-10 uk-text-center">
                <img src="img/14-customer-review/peoples.png" alt="">
            </div>
            <div class="uk-width-small-4-10 uk-width-7-10">
                <h4 class="truncate">Tickazilla company reviews</h4>
                <p>Average experience</p>
            </div>
            <div class="uk-width-small-2-10 uk-hidden-tiny">
                <div class="score" data-score="3"></div>
            </div>
            <div class="uk-width-small-2-10 uk-hidden-tiny uk-text-center">
                <span>412 reviews</span>
            </div>
            <div class="uk-width-small-1-10 uk-width-1-10 uk-text-center">
                <img src="img/14-customer-review/right-arrow.png" alt="">
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-collapse uk-flex-middle review-item">
            <div class="uk-width-small-1-10 uk-width-2-10 uk-text-center">
                <img src="img/14-customer-review/obj-1.png" alt="">
            </div>
            <div class="uk-width-small-4-10 uk-width-7-10">
                <h4 class="truncate">Hotel reviews</h4>
                <p>Very good experience</p>
            </div>
            <div class="uk-width-small-2-10 uk-hidden-tiny">
                <div class="score" data-score="4"></div>
            </div>
            <div class="uk-width-small-2-10 uk-hidden-tiny uk-text-center">
                <span>412 reviews</span>
            </div>
            <div class="uk-width-small-1-10 uk-width-1-10 uk-text-center">
                <img src="img/14-customer-review/right-arrow.png" alt="">
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-collapse uk-flex-middle review-item">
            <div class="uk-width-small-1-10 uk-width-2-10 uk-text-center">
                <img src="img/14-customer-review/tocket.png" alt="">
            </div>
            <div class="uk-width-small-4-10 uk-width-7-10">
                <h4 class="truncate">Event reviews</h4>
                <p>Great experience</p>
            </div>
            <div class="uk-width-small-2-10 uk-hidden-tiny">
                <div class="score" data-score="5"></div>
            </div>
            <div class="uk-width-small-2-10 uk-hidden-tiny uk-text-center">
                <span>412 reviews</span>
            </div>
            <div class="uk-width-small-1-10 uk-width-1-10 uk-text-center">
                <img src="img/14-customer-review/right-arrow.png" alt="">
            </div>
        </a>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>

<script>
    $(document).ready(function(){
        $(".score").raty({
            number: 5,
            numberMax: 5,
            starOn: 'img/8a-hotel-search/8a-star-on.png',
            starOff: 'img/8a-hotel-search/8a-star-off.png',
            readOnly: true,
            score: function(){
                return $(this).attr('data-score')
            }
        });
    });
</script>
