<?php include "include/header.php" ?>

<section id="content">
    <div class="company-review-item mt-10">
        <h4 class="first-title">Hotel reviews</h4>
        <p>Our customers hotel experience</p>

        <div class="uk-grid uk-grid-small uk-flex-middle uk-margin-top" data-uk-grid-margin="">
            <div class="uk-width-6-10">
                <h4 class="green">Excellent Experience</h4>
            </div>
            <div class="uk-width-4-10">
                <div class="score" data-score="5"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-6-10">
                Room comfort:
            </div>
            <div class="uk-width-4-10">
                <div class="score" data-score="4"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-6-10">
                Facilities & dining:
            </div>
            <div class="uk-width-4-10">
                <div class="score" data-score="5"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-6-10">
                Condition & cleanliness:
            </div>
            <div class="uk-width-4-10">
                <div class="score" data-score="4"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-6-10">
                Staff & service:
            </div>
            <div class="uk-width-4-10">
                <div class="score" data-score="4"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-6-10">
                Location:
            </div>
            <div class="uk-width-4-10">
                <div class="score" data-score="4"></div>
            </div>
        </div>
    </div>

    <div class="result-avai-review">
        <h4>94 available hotel reviews, showing 1 - 10</h4>
    </div>

    <div class="filter-sortby">
        <div class="uk-grid uk-grid-small">
            <div class="uk-width-4-10">
                <a href="#" class="uk-button uk-button-primary" data-uk-modal="{target: '#modal-fillter', center: true}">Filters</a>
            </div>
            <div class="uk-width-6-10">
                <div class="uk-button uk-form-select uk-width-7-10 drop-sortby" data-uk-form-select>
                    <span class="val-select"></span>
                    <i class="uk-icon-chevron-down"></i>
                    <select>
                        <option value="">Sort by</option>
                        <option value="">a</option>
                        <option value="">b</option>
                        <option value="">c</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="company-review-item">
        <h4 class="blue-title">Marriott Ghent 4* | Gent | Belgium</h4>

        <div class="uk-grid uk-grid-small uk-flex-middle uk-margin-top" data-uk-grid-margin="">
            <div class="uk-width-small-5-10 uk-width-6-10">
                <h4 class="green">Excellent Experience</h4>
            </div>
            <div class="uk-width-small-3-10 uk-width-4-10">
                <div class="score" data-score="4"></div>
            </div>
            <div class="uk-width-small-2-10 uk-hidden-tiny">
                <span>27 Oct 2015</span>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-1-1">
                W.J.M. van den Bosch | couples | Netherlands
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-small-5-10 uk-width-6-10">
                Room comfort:
            </div>
            <div class="uk-width-small-3-10 uk-width-4-10">
                <div class="score" data-score="5"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-small-5-10 uk-width-6-10">
                Facilities & dining:
            </div>
            <div class="uk-width-small-3-10 uk-width-4-10">
                <div class="score" data-score="4"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-small-5-10 uk-width-6-10">
                Condition & cleanliness:
            </div>
            <div class="uk-width-small-3-10 uk-width-4-10">
                <div class="score" data-score="4"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-small-5-10 uk-width-6-10">
                Staff & service:
            </div>
            <div class="uk-width-small-3-10 uk-width-4-10">
                <div class="score" data-score="4"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-small-5-10 uk-width-6-10">
                Location:
            </div>
            <div class="uk-width-small-3-10 uk-width-4-10">
                <div class="score" data-score="4"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small">
            <div class="uk-width-1-1">
                <span class="blue">Customer Comments:</span>
                <p>
                    The bed was far too hard, a shower in the bath is not the comfort anno 2015.
                    Excellent Staff&Service and breakfast, Parking superb. There is no better location
                    in Gent.
                </p>
            </div>
        </div>
        <div class="uk-grid uk-grid-small">
            <div class="uk-width-1-1">
                <span class="green">Management Response:</span>
                <p class="shorten">
                    Thank you for submitting your feedback. Your tickets were delivered to the Hotel
                    Westin two days prior to the game on Thursday 15 November
                    @ 09:26am by MRW (local courier company). On the afternoon of the event, you
                    informed us that the hotel staff claimed there hadn't been a delivery when in fact
                    your tickets had indeed been delivered to the hotel. Once we gave you the
                    relevant delivered information and the name of the receptionist (Arturo) that
                    signed for the tickets, the hotel staff found the envelope and handed over your
                    tickets. This was all completed within 30 minutes. We feel that it is unfair to
                    apportion blame to our company. We delivered your tickets in accordance with
                    your delivery instructions and in plenty of time before the event. We also had
                    proof of delivery and were available outside our normal office hours to assist and
                    resolve accordingly.
                </p>
            </div>
        </div>
    </div>

    <div class="company-review-item">
        <h4 class="blue-title">Azul Barcelona 3*  |  Barcelona  |  Spain</h4>
        <p>What our customers say about their experience with Tickazilla</p>

        <div class="uk-grid uk-grid-small uk-flex-middle uk-margin-top" data-uk-grid-margin="">
            <div class="uk-width-small-5-10 uk-width-6-10">
                <h4 class="green">Excellent Experience</h4>
            </div>
            <div class="uk-width-small-3-10 uk-width-4-10">
                <div class="score" data-score="4"></div>
            </div>
            <div class="uk-width-small-2-10 uk-hidden-tiny">
                <span>12 Oct 15</span>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-1-1">
                Lindsay Duncan | family with older children | United Kingdom
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-small-5-10 uk-width-6-10">
                Room comfort:
            </div>
            <div class="uk-width-small-3-10 uk-width-4-10">
                <div class="score" data-score="5"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-small-5-10 uk-width-6-10">
                Facilities & dining:
            </div>
            <div class="uk-width-small-3-10 uk-width-4-10">
                <div class="score" data-score="4"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-small-5-10 uk-width-6-10">
                Condition & cleanliness:
            </div>
            <div class="uk-width-small-3-10 uk-width-4-10">
                <div class="score" data-score="4"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-small-5-10 uk-width-6-10">
                Staff & service:
            </div>
            <div class="uk-width-small-3-10 uk-width-4-10">
                <div class="score" data-score="4"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-small-5-10 uk-width-6-10">
                Location:
            </div>
            <div class="uk-width-small-3-10 uk-width-4-10">
                <div class="score" data-score="4"></div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small">
            <div class="uk-width-1-1">
                <span class="blue">Customer Comments:</span>
                <p>
                    Best experience with a ticketing agency...ever. So fun!
                </p>
            </div>
        </div>
    </div>

    <div class="uk-grid wrap-showmore uk-margin-top">
        <div class="uk-width-1-1">
            <div class="uk-text-center">
                <a id="show-more" href="#" class="show-more"><b>Show more...</b></a>
            </div>
        </div>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>

    <!--  Modal Filter  -->
    <div id="modal-fillter" class="uk-modal">
        <div class="uk-modal-dialog">
            <a href="" class="uk-modal-close uk-close"></a>
            <div class="filter-head">
                <h4 class="uk-margin-remove">Filter results</h4>
                <p class="uk-margin-remove">94 hotel reviews</p>
                <a href="#" class="reset">Reset all filters x</a>
            </div>
            <div class="filter-field-row">
                <form class="uk-form uk-form-stacked common-form-modal">
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Hotel name contains</label>
                        <div class="uk-form-controls">
                            <input type="text" id="" class="common-modal-ipt" placeholder="Hotel name...">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="form-input-1">Hotel star rating contains</label>
                        <div class="uk-form-controls">
                            <div class="filter-rate"></div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Country</label>
                        <div class="uk-grid">
                            <div class="uk-width-3-4">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">All Country</option>
                                            <option value="">Vietnam</option>
                                            <option value="">Singapore</option>
                                            <option value="">Laos</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">City</label>
                        <div class="uk-grid">
                            <div class="uk-width-3-4">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">All City</option>
                                            <option value="">City</option>
                                            <option value="">City</option>
                                            <option value="">City</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Rating</label>
                        <div class="uk-grid">
                            <div class="uk-width-3-4">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">All rating</option>
                                            <option value="">4 star</option>
                                            <option value="">3 star</option>
                                            <option value="">2 star</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Type of traveller</label>
                        <div class="uk-grid">
                            <div class="uk-width-3-4">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">All traveller</option>
                                            <option value="">traveller</option>
                                            <option value="">traveller</option>
                                            <option value="">traveller</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--  End Modal Filter  -->
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>

<script>
    $(document).ready(function(){
        $(".score").raty({
            number: 5,
            numberMax: 5,
            starOn: 'img/8a-hotel-search/8a-star-on.png',
            starOff: 'img/8a-hotel-search/8a-star-off.png',
            readOnly: true,
            score: function(){
                return $(this).attr('data-score')
            }
        });

        $(".filter-rate").raty({
            number: 5,
            numberMax: 5,
            starOn: 'img/8a-hotel-search/8a-star-on.png',
            starOff: 'img/8a-hotel-search/8a-star-off.png'
        });

        $(".shorten").shorten({
            "showChars" : 100,
            "moreText"  : '<i class="uk-icon-angle-down"></i>',
            "lessText"  : '<i class="uk-icon-angle-up"></i>'
        });
    });
</script>
