<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Payment help</h1>
        <p>If you encountered a problem with payment or a technical error prevented you from placing a booking, please note the following important information.</p>
    </div>

    <div class="list-payment-help">
        <div class="uk-accordion common-accordion" data-uk-accordion="{collapse: false}">
            <h3 class="uk-accordion-title">
                <span>Q. I cannot submit a payment on your website using my debit/ credit card?</span>
            </h3>
            <div class="uk-accordion-content">
                <p><b>A.</b> Unsuccessful online card payments may result in one of the following three messages provided by our merchant payment provider, WorldPay:</p>
                <p>
                    <b>1. Booking Failed</b>
                </p>
                <p>
                    <b>2. Sorry, your payment card has been declined by your card issuer</b>
                </p>
                <p>
                    <b>3. An available route for this purchase cannot be found (this may be caused by an error in the merchant configuration or by a remote systems failure.)</b>
                </p>
                <p>
                    Your booking was unsuccessful. No money has been taken from your payment card. This could be due to a number of reasons:
                </p>
                <ul>
                    <li>
                        Your payment card is locked to your local currency. The prices on our website
                        are quoted in British Pound Sterling (GBP) but your card issuer will not permit
                        conversions into your local currency.
                    </li>
                    <li>
                        The card details you entered are incorrect or you made a mistake with the
                        billing address that is registered to the payment card.
                    </li>
                    <li>
                        You entered an incorrect personal password when authenticating your
                        payment card using 'Verified by VISA' or 'MasterCard SecureCode'.
                    </li>
                    <li>
                        You have previously registered your payment card with 'Verified by VISA' or
                        'MasterCard SecureCode' but you decided to ignore the validation i.e.
                    </li>
                    <li>
                        Your card issuer suspects fraudulent activity on your payment card and has
                        therefore blocked the transaction due to security concerns.
                    </li>
                    <li>
                        You do not have sufficient funds in your account to cover the full payment
                    </li>
                </ul>
                <p>
                    To ensure successful payment, please carefully review the following steps:
                </p>
                <ol>
                    <li>
                        Your payment card is enabled for foreign currencies i.e. will accept
                        transactions in British Pound Sterling (GBP)
                    </li>
                    <li>
                        You have entered the correct details printed on your payment card.
                    </li>
                    <li>
                        The name and billing address that you entered matches those details that
                        are held with your payment card provider i.e. the name and address where your
                        bank statements are sent to.
                    </li>
                    <li>
                        If you have registered your payment card with 'Verified by VISA' or
                        'MasterCard Secure Code', then you must enter the personal password that you
                        created at the time of registration. If you have forgotten this password, then they
                        do allow you to enter a new password.
                    </li>
                    <li>
                        You have entered the correct personal password for either 'Verified by VISA'
                        or 'MasterCard SecureCode'.
                    </li>
                    <li>
                        You have enough funds in your account to cover the cost of the booking
                    </li>
                </ol>
                <p>
                    If you have followed the above steps then you should try to place the booking
                    again. To begin the booking process again, please close your current web
                    browser, open a new web browser, add the items again to your shopping cart and
                    follow the on screen instructions to complete the booking and payment.
                </p>
                <p>
                    If your payment card declines again, we recommend trying a different payment
                    card or contacting your payment card issuer to question them accordingly.
                </p>
            </div>

            <h3 class="uk-accordion-title">
                <span>Q. Contacting your card issuer</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>Q. Had a problem using our website?</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>
        </div>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
