<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Payment help</h1>
        <p>If you encountered a problem with payment or a technical error prevented you from placing a booking, please note the following important information.</p>
    </div>

    <div class="list-payment-help">
        <div class="uk-accordion common-accordion" data-uk-accordion="{collapse: false}">
            <h3 class="uk-grid uk-grid-small uk-flex-middle other-list-item uk-accordion-title">
                <div class="uk-width-1-10 uk-text-center">
                    <img src="img/16-frequently/obj-1.png" alt="">
                </div>
                <div class="uk-width-8-10">
                    <h4 class="truncate">Sports Event Tickets</h4>
                </div>
            </h3>
            <div class="uk-accordion-content">
                <div class="uk-accordion common-accordion-2">
                    <h3 class="accordion-2-title">Q. When will I receive my event tickets?</h3>
                    <div class="accordion-2-content">
                        <p>
                            A.  This will depend on the delivery options available for your chosen game/ event.
                            Normally, event tickets are only printed and released during the week leading up
                            to the fixture/ event therefore we do not have time to arrange delivery to your
                            home address. Your event tickets will be couriered to your hotel or local collection
                            point no later than noon on the day of the game/ event, where you will be able to
                            pick them up from the reception (unless otherwise stated by Tickazilla).
                        </p>
                    </div>
                    <h3 class="accordion-2-title">Q. Where will my seat be located?</h3>
                    <div class="accordion-2-content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium amet autem
                            beatae commodi cupiditate, delectus explicabo harum inventore labore, magnam minus
                            nobis optio possimus quaerat quasi reprehenderit repudiandae unde. Dignissimos?
                        </p>
                    </div>
                </div>

            </div>

            <h3 class="uk-grid uk-grid-small uk-flex-middle other-list-item uk-accordion-title">
                <div class="uk-width-1-10 uk-text-center">
                    <img src="img/16-frequently/obj-2.png" alt="">
                </div>
                <div class="uk-width-8-10">
                    <h4 class="truncate">Hotel bookings</h4>
                </div>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, debitis, dignissimos. Assumenda cupiditate dolorem dolorum eligendi eveniet exercitationem id iusto laudantium maiores non numquam porro provident, quaerat ratione tenetur ut.</p>
            </div>

            <h3 class="uk-grid uk-grid-small uk-flex-middle other-list-item uk-accordion-title">
                <div class="uk-width-1-10 uk-text-center">
                    <img src="img/16-frequently/obj-3.png" alt="">
                </div>
                <div class="uk-width-8-10">
                    <h4 class="truncate">Booking Amendments</h4>
                </div>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, debitis, dignissimos. Assumenda cupiditate dolorem dolorum eligendi eveniet exercitationem id iusto laudantium maiores non numquam porro provident, quaerat ratione tenetur ut.</p>
            </div>

            <h3 class="uk-grid uk-grid-small uk-flex-middle other-list-item uk-accordion-title">
                <div class="uk-width-1-10 uk-text-center">
                    <img src="img/16-frequently/obj-4.png" alt="">
                </div>
                <div class="uk-width-8-10">
                    <h4 class="truncate">Booking Cancellations</h4>
                </div>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, debitis, dignissimos. Assumenda cupiditate dolorem dolorum eligendi eveniet exercitationem id iusto laudantium maiores non numquam porro provident, quaerat ratione tenetur ut.</p>
            </div>

            <h3 class="uk-grid uk-grid-small uk-flex-middle other-list-item uk-accordion-title">
                <div class="uk-width-1-10 uk-text-center">
                    <img src="img/16-frequently/obj-5.png" alt="">
                </div>
                <div class="uk-width-8-10">
                    <h4 class="truncate">Payment</h4>
                </div>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, debitis, dignissimos. Assumenda cupiditate dolorem dolorum eligendi eveniet exercitationem id iusto laudantium maiores non numquam porro provident, quaerat ratione tenetur ut.</p>
            </div>

            <h3 class="uk-grid uk-grid-small uk-flex-middle other-list-item uk-accordion-title">
                <div class="uk-width-1-10 uk-text-center">
                    <img src="img/16-frequently/obj-6.png" alt="">
                </div>
                <div class="uk-width-8-10">
                    <h4 class="truncate">Travel advice</h4>
                </div>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, debitis, dignissimos. Assumenda cupiditate dolorem dolorum eligendi eveniet exercitationem id iusto laudantium maiores non numquam porro provident, quaerat ratione tenetur ut.</p>
            </div>

            <h3 class="uk-grid uk-grid-small uk-flex-middle other-list-item uk-accordion-title">
                <div class="uk-width-1-10 uk-text-center">
                    <img src="img/16-frequently/obj-7.png" alt="">
                </div>
                <div class="uk-width-8-10">
                    <h4 class="truncate">Travel insurance & protection</h4>
                </div>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, debitis, dignissimos. Assumenda cupiditate dolorem dolorum eligendi eveniet exercitationem id iusto laudantium maiores non numquam porro provident, quaerat ratione tenetur ut.</p>
            </div>

            <h3 class="uk-grid uk-grid-small uk-flex-middle other-list-item uk-accordion-title">
                <div class="uk-width-1-10 uk-text-center">
                    <img src="img/16-frequently/obj-8.png" alt="">
                </div>
                <div class="uk-width-8-10">
                    <h4 class="truncate">Your account</h4>
                </div>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, debitis, dignissimos. Assumenda cupiditate dolorem dolorum eligendi eveniet exercitationem id iusto laudantium maiores non numquam porro provident, quaerat ratione tenetur ut.</p>
            </div>
        </div>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>

<script>
    $(document).ready(function(){
        var accordion_nested = UIkit.accordion(".common-accordion-2", {
            toggle: '.accordion-2-title',
            containers: '.accordion-2-content',
            collapse: false
        });
    });
</script>
