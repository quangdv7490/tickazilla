<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Opening hours</h1>
        <p> Our office opening hours</p>
    </div>

    <div class="block">
        <h4 class="title uk-flex uk-flex-middle">
            <img src="img/17-opening-hour/obj-1.png" alt="">
            Office status
        </h4>
        <div class="uk-grid">
            <div class="uk-width-7-10 uk-container-center">
                <ul class="uk-list list-verify">
                    <li>
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-1-10">
                                <i class="uk-icon-check"></i>
                            </div>
                            <div class="uk-width-9-10">
                                online support available
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-1-10">
                                <i class="uk-icon-times"></i>
                            </div>
                            <div class="uk-width-9-10">
                                call centre closed
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="block">
        <h4 class="title uk-flex uk-flex-middle">
            <img src="img/17-opening-hour/obj-2.png" alt="">
            Opening hours
        </h4>

        <p>
            Our online support team will also be available outside of these hours at
            intermittent intervals to answer web enquiries and process bookings.
        </p>
        <p class="uk-form">
            <select name="" id="">
                <option value="">United Kingdom (GMT 0): Mon - Fri 10:00-17:00</option>
                <option value="">United Kingdom (GMT +7): Mon - Fri 10:00-17:00</option>
            </select>
        </p>
        <p>
            <span>Saturday & Sunday</span>
            Our office telephone lines are closed at the weekend.
        </p>
        <p>
            <span>Emergency telephone assistance</span>
            Customers at their trip destination should text or call on the emergency
            telephone number provided via your online customer account.
        </p>
        <p>
            <span>UK Public/Bank Holidays</span>
            Our offices are closed.<a href="#">Find UK public/ bank holidays ></a>
        </p>
        <p>
            <span>Easter</span>
            Our offices are closed from Good Friday to Easter Monday inclusive
        </p>
        <p>
            <span>Christmas & New Year:</span>
            Generally, our office telephone lines are closed from the close of business
            on the 22nd of December and reopen on the 3rd of January.
        </p>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
