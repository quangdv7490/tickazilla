<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Contact us</h1>
        <p class="uk-margin-bottom">
            Before you contact us, please check our <a href="#">frequently asked questions</a> to see if your
            question has already been answered.
        </p>
        <p>
            Should you still need to contact us, please use one of the following options below.
            We usually respond to email enquiries very quickly.
        </p>
    </div>

    <div class="block-customer has-line">
        <div class="uk-accordion nav-accord">
            <h4 class="uk-accordion-title"><img src="img/18-contactus/obj-1.png" alt=""> E-mail us</h4>
            <div class="uk-accordion-content">
                <p>
                    Please use the form provided below to contact us by email.
                </p>
                <p>
                    <b>Status:</b> <b class="uk-text-danger">online support available</b>
                </p>

                <form action="" class="uk-form uk-form-stacked form-for-contact">
                    <div class="uk-form-row">
                        <input type="text" name="" id="" class="uk-width-small-8-10" placeholder="First name...">
                    </div>
                    <div class="uk-form-row">
                        <input type="text" name="" id="" class="uk-width-small-8-10" placeholder="Last name...">
                    </div>
                    <div class="uk-form-row">
                        <input type="text" name="" id="" class="uk-width-small-8-10" placeholder="E-mail address...">
                    </div>
                    <div class="uk-form-row">
                        <input type="text" name="" id="" class="uk-width-small-8-10" placeholder="Confirm e-mail address...">
                    </div>
                    <div class="uk-form-row">
                        <select name="" id="" class="uk-width-small-8-10">
                            <option value="">Please select subject</option>
                            <option value="">Please select subject</option>
                            <option value="">Please select subject</option>
                        </select>
                    </div>
                    <div class="uk-form-row">
                        <select name="" id="" class="uk-width-small-8-10">
                            <option value="">Booking type</option>
                            <option value="">Booking type</option>
                            <option value="">Booking type</option>
                        </select>
                    </div>
                    <div class="uk-form-row">
                        <select name="" id="" class="uk-width-small-8-10">
                            <option value="">Team name</option>
                            <option value="">Team name</option>
                            <option value="">Team name</option>
                        </select>
                    </div>
                    <div class="uk-form-row">
                        <select name="" id="" class="uk-width-small-8-10">
                            <option value="">Fixture</option>
                            <option value="">Fixture</option>
                            <option value="">Fixture</option>
                        </select>
                    </div>
                    <div class="uk-form-row">
                        <select name="" id="" class="uk-width-small-8-10">
                            <option value="">Number of guest</option>
                            <option value="">Number of guest</option>
                            <option value="">Number of guest</option>
                        </select>
                    </div>
                    <div class="uk-form-row">
                        <textarea name="" id="" class="uk-width-small-8-10" cols="30" rows="10">Please provide details of your enquiry and remember to include your booking reference and/or details of the tickets/ hotel you want to book</textarea>
                    </div>
                    <div class="uk-form-row uk-text-center">
                        <button class="uk-button uk-button-primary">Submit enquiry <i class="uk-icon-angle-right"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="block-customer has-line">
        <div class="uk-accordion nav-accord">
            <h4 class="uk-accordion-title"><img src="img/18-contactus/obj-2.png" alt=""> Telephone</h4>
            <div class="uk-accordion-content">
                <p>
                    We are happy to answer your questions over the telephone.
                </p>
                <div class="uk-text-center">
                    <p>Status: <span class="uk-text-danger">call centre closed</span></p>
                    <a href="#">Opening hours <i class="uk-icon-caret-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="uk-grid uk-grid-collapse uk-flex uk-flex-middle contact-phone-item">
        <div class="uk-width-3-10">
            <b>Local:</b>
        </div>
        <div class="uk-width-4-10 uk-text-center">
            <b class="truncate">0844 804 5040</b>
        </div>
        <div class="uk-width-3-10 uk-text-right">
            <img src="img/18-contactus/right-black.png" alt="">
        </div>
    </a>
    <a href="#" class="uk-grid uk-grid-collapse uk-flex uk-flex-middle contact-phone-item">
        <div class="uk-width-3-10">
            <b class="truncate">International:</b>
        </div>
        <div class="uk-width-4-10 uk-text-center">
            <b class="truncate">+44 844 804 5040</b>
        </div>
        <div class="uk-width-3-10 uk-text-right">
            <img src="img/18-contactus/right-black.png" alt="">
        </div>
    </a>

    <div class="block-customer has-line">
        <div class="uk-accordion nav-accord">
            <h4 class="uk-accordion-title"><img src="img/18-contactus/obj-3.png" alt=""> Call me back</h4>
            <div class="uk-accordion-content">
                <p>
                    Please use the form provided below to request a call back during our normal office opening hours.
                </p>
                <div class="uk-text-center">
                    <p>Status: <span class="uk-text-danger">call centre closed</span></p>
                    <a href="#">Opening hours <i class="uk-icon-caret-right"></i></a>
                </div>

                <form action="" class="uk-form uk-form-stacked form-for-contact uk-margin-top">
                    <div class="uk-form-row">
                        <input type="text" name="" id="" class="uk-width-small-8-10" placeholder="First name...">
                    </div>
                    <div class="uk-form-row">
                        <input type="text" name="" id="" class="uk-width-small-8-10" placeholder="Last name...">
                    </div>
                    <div class="uk-width-small-8-10">
                        <div class="uk-grid">
                            <div class="uk-width-small-1-2 uk-margin-top">
                                <select name="" id="" class="uk-width-1-1">
                                    <option value="">Country…</option>

                                    <option value="">United kingdom +44</option>
                                    <option value="">United kingdom +44</option>
                                </select>
                            </div>
                            <div class="uk-width-small-1-2 uk-margin-top">
                                <input type="text" name="" id="" class="uk-width-1-1" placeholder="Phone no.">
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row uk-margin-top">
                        <select name="" id="" class="uk-width-small-8-10">
                            <option value="">When should we call you?</option>
                            <option value="">When should we call you?</option>
                            <option value="">When should we call you?</option>
                        </select>
                    </div>
                   <!-- <div class="uk-form-row">
                        <input type="text" name="" id="" class="uk-width-small-8-10" placeholder="E-mail address (in case we cannot reach you)">
                    </div>-->
                    <div class="uk-form-row">
                        <input type="text" name="" id="" class="uk-width-small-8-10" placeholder="Confirm e-mail address">
                    </div>
                    <div class="uk-form-row uk-margin-top">
                        <select name="" id="" class="uk-width-small-8-10">
                            <option value="">Please select subject</option>
                            <option value="">Please select subject</option>
                            <option value="">Please select subject</option>
                        </select>
                    </div>
                    <div class="uk-form-row uk-margin-top">
                        <select name="" id="" class="uk-width-small-8-10">
                            <option value="">Booking type</option>
                            <option value="">Booking type</option>
                            <option value="">Booking type</option>
                        </select>
                    </div>
                    <div class="uk-form-row">
                        <select name="" id="" class="uk-width-small-8-10">
                            <option value="">Team name</option>
                            <option value="">Team name</option>
                            <option value="">Team name</option>
                        </select>
                    </div>
                    <div class="uk-form-row">
                        <select name="" id="" class="uk-width-small-8-10">
                            <option value="">Fixture</option>
                            <option value="">Fixture</option>
                            <option value="">Fixture</option>
                        </select>
                    </div>
                    <div class="uk-form-row">
                        <select name="" id="" class="uk-width-small-8-10">
                            <option value="">Number of guest</option>
                            <option value="">Number of guest</option>
                            <option value="">Number of guest</option>
                        </select>
                    </div>
                    <div class="uk-form-row">
                        <textarea name="" id="" class="uk-width-small-8-10" cols="30" rows="10">Please provide details of your enquiry and remember to include your booking reference and/or details of the tickets/ hotel you want to book</textarea>
                    </div>
                    <div class="uk-form-row uk-text-center">
                        <button class="uk-button uk-button-primary">Request call back <i class="uk-icon-angle-right"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
