<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Event & ticket & conditions</h1>
        <p>
            The general conditions for events & tickets can be found below. It is also
            important that you read our <a href="#">general terms & conditions</a> and <a href="#">privacy policy.</a>
        </p>
    </div>

    <div class="list-payment-help">
        <div class="uk-accordion common-accordion" data-uk-accordion>
            <h3 class="uk-accordion-title">
                <span>1. Event dates</span>
            </h3>
            <div class="uk-accordion-content">
                <p>
                    The dates and times for all events are shown as provisional or confirmed. Also,
                    event start times are marked as local.
                    Those events that are marked as provisional may be moved at short notice to
                    alternative event dates or event start times. If an event is marked as provisional
                    then all possible variations to the performance date of each event are displayed
                    online. This information is also available throughout the online booking process.
                    Once the date/ start time for provisional event dates has been confirmed by the
                    event organisers, we'll notify you accordingly via email.
                </p>
            </div>

            <h3 class="uk-accordion-title">
                <span>1. How do provisional event dates affect the duration of my hotel stay?</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>1. Face value of event tickets</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>4. Seating & ticket allocations</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>5. Ticket types provided</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>6. Ticket delivery & collection</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>7. Finding the venue & locating your reserved seats</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>8. Stadium entry & security</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>9. Security measurements, punishment bans & members only at football games</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>10. Flights</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>
        </div>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
