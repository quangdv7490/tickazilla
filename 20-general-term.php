<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>General terms & conditions</h1>
        <p>
            The general terms and conditions for our services can be found below. It is also
            important that you read our <a href="#">event & ticket conditions</a> and <a href="#">privacy policy.</a>
        </p>
    </div>

    <div class="list-payment-help">
        <div class="uk-accordion common-accordion" data-uk-accordion="{collapse: false}">
            <h3 class="uk-accordion-title">
                <span>1. Who we are?</span>
            </h3>
            <div class="uk-accordion-content">
                <p>
                    1.1	We are Football Encounters UK Limited trading online as Tickazilla.com
                    (referred to as "Tickazilla" in these Conditions). Tickazilla's postal address and
                    registered office is 53 Welwyn Park Drive, Beverley High Road, Hull, East
                    Yorkshire, HU6 7DX, United Kingdom. Tickazilla is a trading name of Football
                    Encounters UK Ltd and is registered as a limited company in England and Wales
                    with number 4500142, and VAT registration number 808 8664 89.
                </p>
                <p>
                    1.2	Tickazilla's telephone number is +44 (0) 844 804 5040, fax number is
                    +44(0) 844 804 5040 and e-mail address for you to contact is support@tickazilla.
                    com
                </p>
            </div>

            <h3 class="uk-accordion-title">
                <span>2. Formation of the Contract</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>3. Descriptions</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>4. Calculation of Prices</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>5. Terms of Payment, Variations & Cancellations</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>6. Credit Notes</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>7. Online Account</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>8. Hotel Accommodation</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>9. Delivery and Collection of Event Tickets/Passes</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>10. Complaints</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>11. Limitation of Tickazilla's Liability</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>12. Compensation Limitation</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>13. Passport, Visas, Health and Travel Insurance</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>14. General</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>
        </div>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
