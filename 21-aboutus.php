<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>About Tickazilla</h1>
        <p>
            Find out more about Tickazilla, including information on our history, integrity,
            reliability, partners and key facts.
        </p>
    </div>

    <div class="list-payment-help">
        <div class="uk-accordion common-accordion" data-uk-accordion="{collapse: false}">
            <h3 class="uk-accordion-title">
                <span>1. Introducing Tickazilla</span>
            </h3>
            <div class="uk-accordion-content">
                <p>
                    Tickazilla.com is a trading name of Football Encounters UK Ltd and is registered
                    as a limited company in England and Wales.
                </p>
                <p>
                    With nearly ten years of history and expertise in arranging tickets and hotel
                    accommodation to football events, we have quickly established ourselves as a
                    firm favourite with customers and as one of the World's leading football event
                    specialists.
                </p>
                <p>
                    We became one of the first online football ticket specialists to offer a complete
                    online booking and payment system that allowed our customers to complete their
                    purchase online in one visit. Each year we continue to pour considerable funds
                    and resources into the development of our website to ensure our customers have
                    the best services and buying experience at their fingertips.
                </p>
                <p>
                    Football Encounters was founded in 2002 focusing purely on Football tickets and
                    holidays. Building on the experience gained in the past 9 years we launched a
                    new website in 2009 under the name of 'tickazilla.com' to promote additional
                    sporting and cultural events. In July 2012, we are releasing a new look website
                    that will also feature a new online booking system for discounted hotels across
                    the World.
                </p>
                <p>
                    We believe we are the right company to fulfil your 'leisure travel requirements'
                    and ensure all elements of your booking run like clockwork. Our main aim is to
                    provide a one hundred percent customer satisfaction rate. Our reputation for high
                    quality, value for money packages is growing sale by sale. You will find that our
                    services are second to none and like most of our customers, you will keep coming
                    back to us time and time again.
                </p>
            </div>

            <h3 class="uk-accordion-title">
                <span>2. Our business ethics</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>3. Our experience</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>4. Our customer service</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>5. Our event tickets and hotels</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>6. Peace of mind</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>7. Financial security</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>8. Our customers</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>
        </div>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
