<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Privacy policy</h1>
        <p>
            Our policy is simple: privacy is paramount. We trust it will help you understand
            what data we collect, why we collect it and what we do with it.
        </p>
    </div>

    <div class="list-payment-help">
        <div class="uk-accordion common-accordion" data-uk-accordion="{collapse: false}">
            <h3 class="uk-accordion-title">
                <span>1. Introduction</span>
            </h3>
            <div class="uk-accordion-content">
                <p>
                    Tickazilla has a strict policy of protecting the privacy and security of all visitors to
                    our website. The Privacy Policy outlined below shows the steps we take to ensure
                    that our website is safe and secure and what we do with the information we collect
                    from visitors and customers.
                </p>
            </div>

            <h3 class="uk-accordion-title">
                <span>2. Collection/ use of your information</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>3. Disclosure of your information</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>4. Data security</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>5. Telephone booking</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>
        </div>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
