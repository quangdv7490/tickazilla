<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Cookie policy</h1>
        <p>
            Our policy is simple: privacy is paramount. We trust it will help you understand
            what data we collect, why we collect it and what we do with it.
        </p>
    </div>

    <div class="list-payment-help">
        <div class="uk-accordion common-accordion" data-uk-accordion="{collapse: false}">
            <h3 class="uk-accordion-title">
                <span>1. Introduction to cookies</span>
            </h3>
            <div class="uk-accordion-content">
                <p>
                    A cookie is an element of data that a website can send to your browser, which may
                    then be stored on your computer's hard drive.
                </p>
                <p>
                    These small data files allow you to navigate around the website more easily and
                    are also used for protection to prevent you from unauthorised use and fraudulent
                    activity. It also makes it possible for us to tailor relevant content to fit your needs
                    and improve the overall user experience on our website.
                </p>
                <p>
                    If you are an existing customer or user, we will use a cookie to save your settings
                    which makes it easier for you to navigate around our site and/or login to your
                    online account during future visits.
                </p>
                <p>
                    Cookies may also be used to collect and monitor information from you for statistica
                    l purposes. These types of cookies include third party cookies provided by third
                    party companies such as Google (Google Analytics) which are used only for web
                    analytics and advert-response rates. These are deemed 'performance cookies'
                    (please see definition below) and are for our own exclusive use.
                </p>
                <p>
                    Please note cookies do not enable us or third parties to access your personal
                    information. All the data collected is statistical and remains completely anonymous.
                    The cookies cannot access any personal information held on your computer or
                    device.
                </p>
                <p>
                    You can adjust the settings on your web browser to control cookies or to notify
                    you when you receive a cookie; thus giving you the chance to accept or reject
                    cookies. However, if you do not enable cookies then certain features on our
                    website will not function correctly and your experience may not be what we
                    intended. For example, secure pages, user logins, shopping carts and other
                    essential and non-essential customised features.
                </p>
                <p>
                    The cookies used on our website are detailed below and are based on definitions
                    from the UK's International Chamber of Commerce (ICC) guide for cookie
                    categories:
                </p>
            </div>

            <h3 class="uk-accordion-title">
                <span>2. Strictly necessary cookies</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>3. Performance cookies</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>4. Functionality cookies</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>5. Targeting cookies</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>

            <h3 class="uk-accordion-title">
                <span>6. Further information about cookies</span>
            </h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci laudantium molestiae mollitia nesciunt provident. Asperiores assumenda autem ex optio! Deleniti dolorum eaque facilis impedit modi necessitatibus quaerat quisquam unde voluptas!</p>
            </div>
        </div>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
