<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Password reset</h1>
        <p>Request to reset your password</p>
    </div>

    <div class="block">
        <h4 class="title uk-flex uk-flex-middle uk-flex-center">
            <img src="img/26-password-reset/obj-1.png" alt="">
            Register a new password
        </h4>
        <p>
            Please enter your email address in the box below:
        </p>
        <p>
            We'll send you a link to a secure webpage on our website where you can easily
            reset your password and create a new one.
        </p>
        <div class="uk-grid">
            <div class="uk-width-small-8-10 uk-container-center">
                <form action="" class="uk-form form-for-contact">
                    <div class="uk-form-row">
                        <input type="text" name="" id="" class="uk-width-1-1" placeholder="Email Address">
                    </div>
                    <div class="uk-form-row uk-text-center">
                        <a href="#" class="uk-flex uk-flex-middle uk-flex-center btn-on-design">Reset password <img src="img/right-arrow-white.png" alt=""></a>
                    </div>
                </form>
            </div>
        </div>
        <p>
            You'll normally receive our e-mail within the next few moments but it may take as
            long as 2 hours.
        </p>
        <p>
            If you no longer have access to your e-mail account or can't remember the email
            address you registered, please <a href="#">contact us ></a>
        </p>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
