<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Password reset</h1>
        <p>Request to reset your password</p>
    </div>

    <div class="block">
        <h4 class="title uk-flex uk-flex-middle uk-flex-center">
            <img src="img/26-password-reset/obj-2.png" alt="">
            Email Sent
        </h4>
        <p>
            Instructions to reset your password have been sent to:
        </p>
        <p class="uk-text-center">
            <a href="#" class="insert-user-link">[insert users email address]</a>
        </p>
        <p>
            Please check your email.
        </p>
        <p>
            *Sometimes our emails may filter to your junk/ spam email folder instead of your
            inbox so please check this folder too.
        </p>
        <p class="uk-text-center">
            <a href="#" class="uk-flex uk-flex-middle uk-flex-center btn-on-design">Back to sign-in page <img src="img/right-arrow-white.png" alt=""></a>
        </p>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
