<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Hotel Ibis Barcelona Mollet</h1>
        <div class="score" data-score="4"></div>
        <p>C/ Nicaragua s/n, Mollet Del Vallas, 8100, Spain</p>
        <ul class="uk-subnav uk-subnav-line hotel-subnav air-map">
            <li class="uk-flex uk-flex-middle">Airport of Viladecans</li>
            <li class="uk-flex uk-flex-middle"><img src="img/9a-hotel-profile/9a-maker.png" alt=""><a href="#">Map</a></li>
        </ul>
    </div>

    <div class="block-info-hotel">
        <div class="list-hotel-action">
            <div data-uk-slider="{infinite: false}">
                <div class="uk-slider-container">
                    <ul class="uk-slider uk-grid uk-grid-divider uk-grid-width-1-3 uk-grid-width-small-1-5">
                        <li class="uk-text-center">
                            <a href="#">TripAdvisor<br>reviews</a>
                        </li>
                        <li class="uk-text-center">
                            <a href="#">Choose<br>your rooms</a>
                        </li>
                        <li class="uk-text-center">
                            <a href="#">Hotel<br>information</a>
                        </li>
                        <li class="uk-text-center">
                            <a href="#">Policy &<br>conditions</a>
                        </li>
                        <li class="uk-text-center">
                            <a href="#">Extra<br>fees</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="uk-grid uk-flex-middle" data-uk-slideshow="{animation: 'scroll'}">
            <div class="uk-width-2-10 uk-text-left">
                <a href="" data-uk-slideshow-item="previous"><img src="img/9a-hotel-profile/prev.png" alt=""></a>
            </div>
            <div class="uk-width-6-10">
                <ul class="uk-slideshow" >
                    <li><img src="img/9a-hotel-profile/hotel-slide-01.jpg" width="" height="" alt=""></li>
                    <li><img src="img/9a-hotel-profile/hotel-slide-01.jpg" width="" height="" alt=""></li>
                    <li><img src="img/9a-hotel-profile/hotel-slide-01.jpg" width="" height="" alt=""></li>
                </ul>
            </div>
            <div class="uk-width-2-10 uk-text-right">
                <a href="" data-uk-slideshow-item="next"><img src="img/9a-hotel-profile/next.png" alt=""></a>
            </div>
        </div>
    </div>

    <div class="wrap-hotel-profile">
        <div class="search-hotel search-in-hotel-profile">
            <form action="" class="uk-form uk-form-stacked">
                <div class="uk-form-row">
                    <div class="uk-form-icon uk-width-1-1">
                        <input class="hotel-search" type="text" name="home-search" placeholder="City, country, hotel name…">
                    </div>
                </div>

                <div class="uk-form-row uk-grid">
                    <div class="uk-width-1-2">
                        <label for="" class="uk-form-label label-checkin truncate">Check-in</label>
                        <div class="uk-form-icon uk-width-1-1">
                            <i class="uk-icon-calendar calendar-ic"></i>
                            <input type="text" name="" id="check-in-hotel" class="ipt-checkin uk-width-1-1" >
                        </div>
                    </div>
                    <div class="uk-width-1-2">
                        <label for="" class="uk-form-label label-checkin truncate">Check-out</label>
                        <div class="uk-form-icon uk-width-1-1">
                            <i class="uk-icon-calendar calendar-ic"></i>
                            <input type="text" name="" id="hotel-check-out" class="ipt-checkin uk-width-1-1">
                        </div>
                    </div>
                </div>

                <div class="uk-form-row uk-grid">
                    <div class="uk-width-1-3">
                        <label for="" class="uk-form-label label-cm truncate">Nights</label>
                        <div class="uk-button drop-night-room uk-form-select uk-width-1-1" data-uk-form-select>
                            <span></span>
                            <i class="uk-icon-chevron-down"></i>
                            <select>
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-1-3">
                        <label for="" class="uk-form-label label-cm truncate">Rooms</label>
                        <div class="uk-button drop-night-room uk-form-select uk-width-1-1" data-uk-form-select>
                            <span></span>
                            <i class="uk-icon-chevron-down"></i>
                            <select>
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="uk-form-row uk-grid uk-flex-middle">
                    <div class="uk-width-1-5">
                        <label class="uk-form-label label-cm">Room 1:</label>
                    </div>
                    <div class="uk-width-2-5">
                        <label for="" class="uk-form-label label-cm truncate">Adults (13+)</label>
                        <div class="uk-button drop-select-room uk-form-select uk-width-1-1" data-uk-form-select>
                            <img class="adult" src="img/adult.png" alt="">
                            <span></span>
                            <i class="uk-icon-chevron-down"></i>
                            <select>
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-2-5">
                        <label for="" class="uk-form-label label-cm truncate">Children (0-12)</label>
                        <div class="uk-button drop-select-room uk-form-select uk-width-1-1" data-uk-form-select>
                            <img class="children" src="img/child.png" alt="">
                            <span></span>
                            <i class="uk-icon-chevron-down"></i>
                            <select>
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="uk-form-row uk-grid uk-flex-middle">
                    <div class="uk-width-1-5">
                        <label class="uk-form-label label-cm">Room 2:</label>
                    </div>
                    <div class="uk-width-2-5">
                        <label for="" class="uk-form-label label-cm truncate">Adults (13+)</label>
                        <div class="uk-button drop-select-room uk-form-select uk-width-1-1" data-uk-form-select>
                            <img class="adult" src="img/adult.png" alt="">
                            <span></span>
                            <i class="uk-icon-chevron-down"></i>
                            <select>
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-2-5">
                        <label for="" class="uk-form-label label-cm truncate">Children (0-12)</label>
                        <div class="uk-button drop-select-room uk-form-select uk-width-1-1" data-uk-form-select>
                            <img class="children" src="img/child.png" alt="">
                            <span></span>
                            <i class="uk-icon-chevron-down"></i>
                            <select>
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="uk-form-row">
                    <button class="hotel-submit profile-hotel-submit"><i class="uk-icon-search"></i> Search</button>
                </div>
            </form>
        </div>
    </div>

    <div class="other-list-review">
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-hotel-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Hotel description</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-map-maker-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Hotel location</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-google-map-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Google map</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-H-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Hotel facilities</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-room-faci-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Room facilities</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-hotel-poli-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Hotel policies & conditions</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-extra-card-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Extra fees & taxes payable by guests</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-tripad-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">TripAdvisor reviews</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>

<script type="text/javascript">
    $(document).ready(function(){
        $(".score").raty({
            number: 5,
            numberMax: 5,
            starOn: 'img/8a-hotel-search/8a-star-on.png',
            starOff: 'img/8a-hotel-search/8a-star-off.png',
            readOnly: true,
            score: function(){
                return $(this).attr('data-score')
            }
        });

        $(function(){
            $("#check-in-hotel").datepicker({
                position: 'bottom left',
                language: 'en',
                autoClose: true
            });

            $("#hotel-check-out").datepicker({
                position: 'bottom right',
                language: 'en',
                autoClose: true
            });
        })
    });
</script>
