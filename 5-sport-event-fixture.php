<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Football (Soccer)</h1>
        <h1>San Cristobal de la Laguna</h1>
        <p>Tickets and hotel packages</p>
    </div>

    <div class="filter-select-page">
        <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-2-4">
                <a href="#" class="uk-button btn-filter" data-uk-modal="{target: '#modal-filter', center: true}">Filters</a>
            </div>
        </div>
    </div>
    
    <div class="wrap-list-fixture">
        <div class="avaiable-fixture">
            <div class="uk-grid">
                <div class="uk-width-1-2">
                    <b>17 available fixtures</b>
                </div>
                <div class="uk-width-1-2 uk-text-right">
                    <b>Prices:</b> <span class="currency">£ GBP</span>
                </div>
            </div>
        </div>

        <div class="list-fixture">
            <div class="fixture-item">
                <div class="uk-grid uk-grid-small">
                    <div class="uk-width-2-10 uk-width-small-2-10">
                        <div class="calen-fixture">
                            <img src="img/clubs-icon/barca-ic.png" alt="">
                            <div class="item-cal">
                                <div class="head">
                                    sep'15
                                </div>
                                <div class="body">
                                    <span>Thu</span>
                                    <span>07</span>
                                </div>
                                <div class="foot">
                                    00:00
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-8-10 uk-width-small-7-10">
                        <div class="info-fixture">
                            <h4>FC Barcelona <span>vs</span> EA7 Emporio Armani Olimpia</h4>
                            <ul class="uk-list">
                                <li>Spanish Primera La Liga BBVA</li>
                                <li>San Cristobal de la Laguna</li>
                                <li>Spain</li>
                            </ul>
                            <div class="uk-grid uk-flex-middle uk-grid-divider">
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-1-1 uk-text-center">
                                            <img class="option-1" src="img/clubs-icon/option-1.png" alt="">
                                            <span>Tickets</span>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <div class="option-span">
                                                <span>from:</span>
                                                <span>£1,051</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-1-1 uk-text-center">
                                            <img class="option-2" src="img/clubs-icon/option-2.png" alt="">
                                            <span>Ticket & Hotel</span>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <div class="option-span">
                                                <span>from:</span>
                                                <span>£1,975</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-1 uk-margin-top">
                        <span class="confirmed">Date & time: To be confirmed</span>
                        <img src="img/confirm-ic.png" alt="">
                    </div>
                </div>
            </div>
            <div class="fixture-item">
                <div class="uk-grid uk-grid-small">
                    <div class="uk-width-2-10 uk-width-small-2-10">
                        <div class="calen-fixture">
                            <img src="img/clubs-icon/barca-ic.png" alt="">
                            <div class="item-cal">
                                <div class="head">
                                    sep'15
                                </div>
                                <div class="body">
                                    <span>Thu</span>
                                    <span>07</span>
                                </div>
                                <div class="foot">
                                    00:00
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-8-10 uk-width-small-7-10">
                        <div class="info-fixture">
                            <h4>FC Barcelona <span>vs</span> EA7 Emporio Armani Olimpia</h4>
                            <ul class="uk-list">
                                <li>Spanish Primera La Liga BBVA</li>
                                <li>San Cristobal de la Laguna</li>
                                <li>Spain</li>
                            </ul>
                            <div class="uk-grid uk-flex-middle uk-grid-divider">
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-1-1 uk-text-center">
                                            <img class="option-1" src="img/clubs-icon/option-1.png" alt="">
                                            <span>Tickets</span>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <div class="option-span">
                                                <span>from:</span>
                                                <span>£1,051</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-1-1 uk-text-center">
                                            <img class="option-2" src="img/clubs-icon/option-2.png" alt="">
                                            <span>Ticket & Hotel</span>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <div class="option-span">
                                                <span>from:</span>
                                                <span>£1,975</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-1 uk-margin-top">
                        <span class="confirmed">Date & time: To be confirmed</span>
                        <img src="img/confirm-ic.png" alt="">
                    </div>
                </div>
            </div>
            <div class="fixture-item">
                <div class="uk-grid uk-grid-small">
                    <div class="uk-width-2-10 uk-width-small-2-10">
                        <div class="calen-fixture">
                            <img src="img/clubs-icon/barca-ic.png" alt="">
                            <div class="item-cal">
                                <div class="head">
                                    sep'15
                                </div>
                                <div class="body">
                                    <span>Thu</span>
                                    <span>07</span>
                                </div>
                                <div class="foot">
                                    00:00
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-8-10 uk-width-small-7-10">
                        <div class="info-fixture">
                            <h4>FC Barcelona <span>vs</span> EA7 Emporio Armani Olimpia</h4>
                            <ul class="uk-list">
                                <li>Spanish Primera La Liga BBVA</li>
                                <li>San Cristobal de la Laguna</li>
                                <li>Spain</li>
                            </ul>
                            <div class="uk-grid uk-flex-middle uk-grid-divider">
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-1-1 uk-text-center">
                                            <img class="option-1" src="img/clubs-icon/option-1.png" alt="">
                                            <span>Tickets</span>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <div class="option-span">
                                                <span>from:</span>
                                                <span>£1,051</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-1-1 uk-text-center">
                                            <img class="option-2" src="img/clubs-icon/option-2.png" alt="">
                                            <span>Ticket & Hotel</span>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <div class="option-span">
                                                <span>from:</span>
                                                <span>£1,975</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-1 uk-margin-top">
                        <span class="confirmed">Date & time: To be confirmed</span>
                        <img src="img/confirm-ic.png" alt="">
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-grid wrap-showmore uk-margin-top">
            <div class="uk-width-1-1">
                <div class="uk-text-center">
                    <a id="show-more" href="#" class="show-more"><b>More fixture options…</b></a>
                </div>
            </div>
        </div>
    </div>

    <div class="ticket-info">
        <p>
            * Excluding ticket delivery & credit card fees <br/> Ticket & hotel price per person | 2 adults <br/> twin/ double room | 1 night
        </p>
    </div>

    <div class="company-review">
        <div class="head">
            <h4>Tickazilla reviews</h4>
            <p>Our customers experience</p>
            <span>97% recommend Tickazilla</span>
        </div>
        <div class="body">
            <div class="uk-grid uk-grid-collapse uk-flex-middle" data-uk-margin>
                <div class="uk-width-6-10">
                    <h4 class="great-title">Great Experience</h4>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Quality of website:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Accuracy of information:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Customer support:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">24hr assistance telephone:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Prices compared to others:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>

                <div class="uk-width-1-1">
                    <a href="#" class="link-form-ticka"><i class="uk-icon-caret-right"></i> From 26 Customer reviews</a>
                </div>
            </div>
        </div>
    </div>

    <?php include "include/block-follow.php"; ?>

    <!--  Modal Filter  -->
    <div id="modal-filter" class="uk-modal">
        <div class="uk-modal-dialog">
            <a href="" class="uk-modal-close uk-close"></a>
            <div class="filter-head">
                <h4 class="uk-margin-remove">Filter results</h4>
                <p class="uk-margin-remove">25 fixture results</p>
                <a href="#" class="reset">Reset all filters x</a>
            </div>
            <div class="filter-field-row">
                <form class="uk-form uk-form-stacked common-form-modal">
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="form-input-1">Team name contains</label>
                        <div class="uk-form-controls">
                            <input type="text" id="form-input-1" class="common-modal-ipt" placeholder="Sports team name…">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Team</label>
                        <div class="uk-grid">
                            <div class="uk-width-3-4">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">All football team</option>
                                            <option value="">Barcelona</option>
                                            <option value="">Real Mandrid</option>
                                            <option value="">Manchester Utd</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="form-input-1">Ticket price (per person)</label>
                        <div class="uk-grid">
                            <div class="uk-width-1-2 uk-width-small-1-4">
                                <span>Min (£)</span>
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">0</option>
                                            <option value="">1</option>
                                            <option value="">2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2 uk-width-small-1-4">
                                <span>Max (£)</span>
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">500+</option>
                                            <option value="">400+</option>
                                            <option value="">300+</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Month</label>
                        <div class="uk-grid">
                            <div class="uk-width-3-4">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">All Month</option>
                                            <option value="">Jan</option>
                                            <option value="">Feb</option>
                                            <option value="">Mar</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Country</label>
                        <div class="uk-grid">
                            <div class="uk-width-3-4">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">All Countries</option>
                                            <option value="">Vietnam</option>
                                            <option value="">Singapore</option>
                                            <option value="">Laos</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">City</label>
                        <div class="uk-grid">
                            <div class="uk-width-3-4">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">All City</option>
                                            <option value="">Vietnam</option>
                                            <option value="">Singapore</option>
                                            <option value="">Laos</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Home or away fixtures</label>
                        <div class="uk-grid">
                            <div class="uk-width-3-4">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">All Fixtures</option>
                                            <option value="">Vietnam</option>
                                            <option value="">Singapore</option>
                                            <option value="">Laos</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">League or cup name</label>
                        <div class="uk-grid">
                            <div class="uk-width-3-4">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">All leagues and cups</option>
                                            <option value="">Vietnam</option>
                                            <option value="">Singapore</option>
                                            <option value="">Laos</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--  End Modal Filter  -->
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
