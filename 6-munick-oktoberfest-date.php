<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Munich Oktoberfest</h1>
        <p>Tickets & hotel packages</p>
    </div>

    <div class="filter-select-page">
        <div class="uk-grid">
            <div class="uk-width-2-4">
                <div class="uk-button uk-form-select uk-width-1-1 dropdown-clubs" data-uk-form-select>
                    <span class="val-select"></span>
                    <i class="uk-icon-chevron-down"></i>
                    <select>
                        <option value="">Sort by</option>
                        <option value="">Name</option>
                        <option value="">Date</option>
                        <option value="">Bla Bla</option>
                    </select>
                </div>
            </div>
            <div class="uk-width-2-4">
                <div class="uk-button uk-form-select uk-width-1-1 dropdown-clubs" data-uk-form-select>
                    <span class="val-select"></span>
                    <i class="uk-icon-chevron-down"></i>
                    <select>
                        <option value="">Select days</option>
                        <option value="">Mon</option>
                        <option value="">Tue</option>
                        <option value="">Wed</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-list-fixture">
        <div class="avaiable-fixture">
            <div class="uk-grid">
                <div class="uk-width-1-2">
                    <b>16 available dates</b>
                </div>
                <div class="uk-width-1-2 uk-text-right">
                    <b>Prices:</b> <span class="currency">£ GBP</span>
                </div>
            </div>
        </div>

        <div class="list-fixture">
            <div class="fixture-item">
                <div class="uk-grid uk-grid-small">
                    <div class="uk-width-2-10 uk-width-small-2-10">
                        <div class="calen-fixture">
                            <div class="item-cal uk-margin-top-remove">
                                <div class="head">
                                    sep'15
                                </div>
                                <div class="body">
                                    <span>Thu</span>
                                    <span>07</span>
                                </div>
                                <div class="foot base">
                                    Times vary
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-8-10 uk-width-small-7-10">
                        <div class="info-fixture">
                            <h4>Munich Oktoberfest</h4>
                            <ul class="uk-list uk-margin-remove">
                                <li>Beer tents</li>
                                <li>Reserved table seats</li>
                            </ul>
                            <div class="uk-grid uk-flex-middle uk-grid-divider">
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-1-1 uk-text-center">
                                            <img class="option-1" src="img/clubs-icon/option-1.png" alt="">
                                            <span>Tickets</span>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <div class="option-span">
                                                <span>from:</span>
                                                <span>£1,051</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-1-1 uk-text-center">
                                            <img class="option-2" src="img/clubs-icon/option-2.png" alt="">
                                            <span>Tickets & Hotel</span>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <div class="option-span">
                                                <span>from:</span>
                                                <span>£1,975</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="fixture-item">
                <div class="uk-grid uk-grid-small">
                    <div class="uk-width-2-10 uk-width-small-2-10">
                        <div class="calen-fixture">
                            <div class="item-cal uk-margin-top-remove">
                                <div class="head">
                                    sep'15
                                </div>
                                <div class="body">
                                    <span>Thu</span>
                                    <span>07</span>
                                </div>
                                <div class="foot base">
                                    Times vary
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-8-10 uk-width-small-7-10">
                        <div class="info-fixture">
                            <h4>Munich Oktoberfest</h4>
                            <ul class="uk-list uk-margin-remove">
                                <li>Beer tents</li>
                                <li>Reserved table seats</li>
                            </ul>
                            <div class="uk-grid uk-flex-middle uk-grid-divider">
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-1-1 uk-text-center">
                                            <img class="option-1" src="img/clubs-icon/option-1.png" alt="">
                                            <span>Tickets</span>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <div class="option-span">
                                                <span>from:</span>
                                                <span>£1,051</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-1-1 uk-text-center">
                                            <img class="option-2" src="img/clubs-icon/option-2.png" alt="">
                                            <span>Tickets & Hotel</span>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <div class="option-span">
                                                <span>from:</span>
                                                <span>£1,975</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fixture-item">
                <div class="uk-grid uk-grid-small">
                    <div class="uk-width-2-10 uk-width-small-2-10">
                        <div class="calen-fixture">
                            <div class="item-cal uk-margin-top-remove">
                                <div class="head">
                                    sep'15
                                </div>
                                <div class="body">
                                    <span>Thu</span>
                                    <span>07</span>
                                </div>
                                <div class="foot base">
                                    Times vary
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-8-10 uk-width-small-7-10">
                        <div class="info-fixture">
                            <h4>Munich Oktoberfest</h4>
                            <ul class="uk-list uk-margin-remove">
                                <li>Beer tents</li>
                                <li>Reserved table seats</li>
                            </ul>
                            <div class="uk-grid uk-flex-middle uk-grid-divider">
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-1-1 uk-text-center">
                                            <img class="option-1" src="img/clubs-icon/option-1.png" alt="">
                                            <span>Tickets</span>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <div class="option-span">
                                                <span>from:</span>
                                                <span>£1,051</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-1-1 uk-text-center">
                                            <img class="option-2" src="img/clubs-icon/option-2.png" alt="">
                                            <span>Tickets & Hotel</span>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <div class="option-span">
                                                <span>from:</span>
                                                <span>£1,975</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="fixture-item">
                <div class="uk-grid uk-grid-small">
                    <div class="uk-width-2-10 uk-width-small-2-10">
                        <div class="calen-fixture">
                            <div class="item-cal uk-margin-top-remove">
                                <div class="head">
                                    sep'15
                                </div>
                                <div class="body">
                                    <span>Thu</span>
                                    <span>07</span>
                                </div>
                                <div class="foot base">
                                    Times vary
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-8-10 uk-width-small-7-10">
                        <div class="info-fixture">
                            <h4>Munich Oktoberfest</h4>
                            <ul class="uk-list uk-margin-remove">
                                <li>Beer tents</li>
                                <li>Reserved table seats</li>
                            </ul>
                            <div class="uk-grid uk-flex-middle uk-grid-divider">
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-1-1 uk-text-center">
                                            <img class="option-1" src="img/clubs-icon/option-1.png" alt="">
                                            <span>Tickets</span>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <div class="option-span">
                                                <span>from:</span>
                                                <span>£1,051</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-1-1 uk-text-center">
                                            <img class="option-2" src="img/clubs-icon/option-2.png" alt="">
                                            <span>Tickets & Hotel</span>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <div class="option-span">
                                                <span>from:</span>
                                                <span>£1,975</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="uk-grid wrap-showmore uk-margin-top">
            <div class="uk-width-1-1">
                <div class="uk-text-center">
                    <a id="show-more" href="#" class="show-more"><b>More date options...</b></a>
                </div>
            </div>
        </div>
    </div>

    <div class="ticket-info">
        <p>
            * Excluding ticket delivery & credit card fees <br/> Ticket & hotel price per person | 2 adults <br/> twin/ double room | 1 night
        </p>
    </div>

    <div class="company-review">
        <div class="head">
            <h4>Tickazilla reviews</h4>
            <p>Our customers experience</p>
            <span>97% recommend Tickazilla</span>
        </div>
        <div class="body">
            <div class="uk-grid uk-grid-collapse uk-flex-middle" data-uk-margin>
                <div class="uk-width-6-10">
                    <h4 class="great-title">Great Experience</h4>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Quality of website:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Accuracy of information:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Customer support:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">24hr emergency assistance:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Prices compared to others:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>
                <div class="uk-width-1-1">
                    <a href="#" class="link-form-ticka"><i class="uk-icon-caret-right"></i> From 26 Customer reviews</a>
                </div>
            </div>
        </div>
    </div>

    <?php include "include/block-follow.php"; ?>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
