<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>FC Barcelona vs.<br>EA7 Emporio Armani Olimpia Milano</h1>
    </div>

    <div class="club-vs">
        <h4>Italian Serie A  |  Round 29</h4>
        <h4>Stadio Giuseppe Meazza, San Siro, Milan, Italy</h4>
        <p class="time">30 Nov 2015 or 01, 02, 03 Dec 2015</p>
        <div class="confirm-date-time">
            <span>Date & time: to be confirmed</span>
            <img src="img/confirm-ic.png" alt="">
        </div>
    </div>
    
    <div class="club-stadium">
        <div class="img-stadium">
            <img src="img/clubs-icon/stadium.png" alt="">
        </div>
        <span class="noti">Hover over stadium layout to zoom in</span>
        <img src="img/loop.png" alt="">
    </div>

    <div class="event-status">
        <div class="head">
            <h4>Event Status</h4>
            <p>Please check back for updates</p>
        </div>
    </div>

    <div class="box-contact">
        <h4>Contact Us</h4>
        <p>Tickets are on a request basis.</p>
        <p>Please contact us with your requirements.</p>
    </div>

    <div class="avaiable-event">
        <div class="uk-grid uk-flex-middle">
            <div class="uk-width-1-2">
                <h4 class="event truncate">Tickets available</h4>
            </div>
            <div class="uk-width-1-2 uk-text-right">
                <b>Prices:</b> <span class="currency">£ GBP</span>
            </div>
        </div>
    </div>

    <div class="list-ticket-avai">
        <div class="ticket-avai">
            <div class="info">
                <h4>Palco VIP Gold with Hospitality & Tour</h4>
                <h4>Home/ Neutral Section.</h4>
            </div>
            <div class="uk-grid uk-grid-divider">            
                <div class="uk-width-2-3">
                    
                    <div class="icon-square">
                        <ul class="uk-subnav">
                            <li><p class="square-event color-1"></p></li>
                            <li><p class="square-event color-2"></p></li>
                            <li><img src="img/confirm-ic.png" alt=""></li>
                        </ul>
                    </div>
                    <span class="ticket-condition"><i class="uk-icon-caret-right"></i> Ticket conditions</span>
                </div>
                <div class="uk-width-1-3 uk-flex uk-flex-middle uk-flex-center nth-2-divider">
                    <div class="act">
                        <span>£1,089</span>
                        <a href="#" class="btn-book-now">Book now <i class="uk-icon-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="ticket-avai">
            <div class="info">
                <h4>Palco VIP Gold with Hospitality & Tour</h4>
                <h4>Home/ Neutral Section.</h4>
            </div>
            <div class="uk-grid uk-grid-divider">            
                <div class="uk-width-2-3">
                    
                    <div class="icon-square">
                        <ul class="uk-subnav">
                            <li><p class="square-event color-1"></p></li>
                            <li><p class="square-event color-2"></p></li>
                            <li><img src="img/confirm-ic.png" alt=""></li>
                        </ul>
                    </div>
                    <span class="ticket-condition"><i class="uk-icon-caret-right"></i> Ticket conditions</span>
                </div>
                <div class="uk-width-1-3 uk-flex uk-flex-middle uk-flex-center nth-2-divider">
                    <div class="act">
                        <span>£1,089</span>
                        <a href="#" class="btn-book-now">Book now <i class="uk-icon-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="ticket-avai">
            <div class="info">
                <h4>Palco VIP Gold with Hospitality & Tour</h4>
                <h4>Home/ Neutral Section.</h4>
            </div>
            <div class="uk-grid uk-grid-divider">            
                <div class="uk-width-2-3">
                    
                    <div class="icon-square">
                        <ul class="uk-subnav">
                            <li><p class="square-event color-1"></p></li>
                            <li><p class="square-event color-2"></p></li>
                            <li><img src="img/confirm-ic.png" alt=""></li>
                        </ul>
                    </div>
                    <span class="ticket-condition"><i class="uk-icon-caret-right"></i> Ticket conditions</span>
                </div>
                <div class="uk-width-1-3 uk-flex uk-flex-middle uk-flex-center nth-2-divider">
                    <div class="act">
                        <span>£1,089</span>
                        <a href="#" class="btn-book-now">Book now <i class="uk-icon-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="uk-grid wrap-showmore uk-margin-top">
        <div class="uk-width-1-1">
            <div class="uk-text-center">
                <a id="show-more" href="#" class="show-more"><b>More ticket options…</b></a>
            </div>
        </div>
    </div>

    <div class="event-status pb-5">
        <div class="head">
            <h4>Further information</h4>
            <p>Please read this important information</p>
        </div>
    </div>

    <div class="list-blue-link uk-padding-top-remove">
        <uk class="uk-list">
            <li><a href="#"><i class="uk-icon-caret-right"></i> Event & ticket conditions</a></li>
            <li><a href="#"><i class="uk-icon-caret-right"></i> General terms & conditions</a></li>
            <li><a href="#"><i class="uk-icon-caret-right"></i> Frequently asked questions</a></li>
        </uk>
    </div>

    <div class="company-review">
        <div class="head">
            <h4>Tickazilla company reviews</h4>
            <p>What our customers say about their experience</p>
            <span>97% recommend Tickazilla</span>
        </div>
        <div class="body">
            <div class="uk-grid uk-grid-collapse uk-flex-middle" data-uk-margin>
                <div class="uk-width-6-10">
                    <h4 class="great-title">Great Experience</h4>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Ticket delivery or collection:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Seat & view at event:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Venue facilities & staff:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Venue atmosphere:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Enjoyment at event:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>
                <div class="uk-width-1-1">
                    <a href="#" class="link-form-ticka"><i class="uk-icon-caret-right"></i> From 26 customer reviews</a>
                </div>
            </div>
        </div>
    </div>

    <?php include "include/block-follow.php" ?>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
