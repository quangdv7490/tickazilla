<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Munich Oktoberfest tent tickets</h1>
        <p>Reserved table seats in beer tents</p>
    </div>

    <div class="club-vs">
        <p class="time">Munich Oktoberfest</p>
        <p class="time">Theresienwiesse, Munich, Germany</p>
        <p class="uk-margin-top"><b>19 September  2015</b></p>
        <div class="confirm-date-time">
            <span class="time">*Various start times</span>
            <img src="img/confirm-ic.png" alt="">
        </div>
    </div>

    <div class="club-stadium">
        <span class="noti">Sorry, we do not have a venue layout<br>Please see ticket descriptions</span>
    </div>

    <div class="event-status">
        <div class="head">
            <h4>Event Status</h4>
            <p>Please continue to check this webpage for updates</p>
        </div>
    </div>

    <div class="box-contact">
        <h4>Contact Us</h4>
        <p>Tickets are on a request basis<br>Please contact us with your requirements</p>
    </div>

    <div class="avaiable-event">
        <div class="uk-grid uk-flex-middle">
            <div class="uk-width-1-2">
                <h4 class="event truncate">Tickets available</h4>
            </div>
            <div class="uk-width-1-2 uk-text-right">
                <b>Prices:</b> <span class="currency">£ GBP</span>
            </div>
        </div>
    </div>

    <div class="list-ticket-avai">
        <div class="ticket-avai">
            <div class="info">
                <h4>Any of the Twelve Big Beer Tents</h4>
            </div>
            <div class="uk-grid uk-grid-divider uk-flex-middle">
                <div class="uk-width-2-3">
                    <div class="uk-grid ">
                        <div class="uk-width-1-1">
                            <div class="info">
                                <p>Table Seat</p>
                                <p>Afternoon (11:30-16:00)</p>
                                <a href="#"><img src="img/confirm-ic.png" alt="" class=""></a>
                                <a href="#" class="link-form-ticka uk-block"><i class="uk-icon-caret-right"></i> Terms & Conditions</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-3 uk-flex-center nth-2-divider">
                    <div class="act">
                        <span>£1,209</span>
                        <a href="#" class="btn-book-now">Book now <i class="uk-icon-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="ticket-avai">
            <div class="info">
                <h4>Hofbrau Tent</h4>
            </div>
            <div class="uk-grid uk-grid-divider uk-flex-middle">
                <div class="uk-width-2-3">
                    <div class="uk-grid ">
                        <div class="uk-width-1-1">
                            <div class="info">
                                <p>Table (10 x Seats)</p>
                                <p>Evening (16:30-23:00)</p>
                                <a href="#"><img src="img/confirm-ic.png" alt="" class=""></a>
                                <a href="#" class="link-form-ticka uk-block"><i class="uk-icon-caret-right"></i> Terms & Conditions</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-3 uk-flex-center nth-2-divider">
                    <div class="act">
                        <span>£1,226</span>
                        <a href="#" class="btn-book-now">Book now <i class="uk-icon-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="ticket-avai">
            <div class="info">
                <h4>Any of the Twelve Big Beer Tents</h4>
            </div>
            <div class="uk-grid uk-grid-divider uk-flex-middle">
                <div class="uk-width-2-3">
                    <div class="uk-grid ">
                        <div class="uk-width-1-1">
                            <div class="info">
                                <p>Table Seat</p>
                                <p>Afternoon (11:30-16:00)</p>
                                <a href="#"><img src="img/confirm-ic.png" alt="" class=""></a>
                                <a href="#" class="link-form-ticka uk-block"><i class="uk-icon-caret-right"></i> Terms & Conditions</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-3 uk-flex-center nth-2-divider">
                    <div class="act">
                        <span>£1,209</span>
                        <a href="#" class="btn-book-now">Book now <i class="uk-icon-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="ticket-avai">
            <div class="info">
                <h4>Hofbrau Tent</h4>
            </div>
            <div class="uk-grid uk-grid-divider uk-flex-middle">
                <div class="uk-width-2-3">
                    <div class="uk-grid ">
                        <div class="uk-width-1-1">
                            <div class="info">
                                <p>Table (10 x Seats)</p>
                                <p>Evening (16:30-23:00)</p>
                                <a href="#"><img src="img/confirm-ic.png" alt="" class=""></a>
                                <a href="#" class="link-form-ticka uk-block"><i class="uk-icon-caret-right"></i> Terms & Conditions</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-3 uk-flex-center nth-2-divider">
                    <div class="act">
                        <span>£1,226</span>
                        <a href="#" class="btn-book-now">Book now <i class="uk-icon-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="uk-grid wrap-showmore uk-margin-top">
        <div class="uk-width-1-1">
            <div class="uk-text-center">
                <a id="show-more" href="#" class="show-more"><b>More ticket options...</b></a>
            </div>
        </div>
    </div>

    <div class="event-status pb-5">
        <div class="head">
            <h4>Further information</h4>
            <p>Please read this important information</p>
        </div>
    </div>

    <div class="list-blue-link uk-padding-top-remove">
        <uk class="uk-list">
            <li><a href="#"><i class="uk-icon-caret-right"></i> General terms & conditions</a></li>
            <li><a href="#"><i class="uk-icon-caret-right"></i> Frequently asked questions</a></li>
        </uk>
    </div>

    <div class="company-review">
        <div class="head">
            <h4>FC Barcelona reviews</h4>
            <p>What our customers say about their experience</p>
            <span>97% recommend Tickazilla</span>
        </div>
        <div class="body">
            <div class="uk-grid uk-grid-collapse uk-flex-middle" data-uk-margin>
                <div class="uk-width-6-10">
                    <h4 class="great-title">Great Experience</h4>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Ticket delivery or collection:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Seat and view provided at venue:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Venue facilities and staff:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Venue atmosphere:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>
                <div class="uk-width-6-10">
                    <p class="quality">Enjoyment at event:</p>
                </div>
                <div class="uk-width-4-10">
                    <div class="star-rate">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span class="in-active"></span>
                    </div>
                </div>

                <div class="uk-width-1-1">
                    <a href="#" class="link-form-ticka"><i class="uk-icon-caret-right"></i> From 56 customer reviews</a>
                </div>
            </div>
        </div>
    </div>

    <?php include "include/block-follow.php" ?>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>
