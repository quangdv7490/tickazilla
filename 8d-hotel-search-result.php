<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Group bookings for hotels</h1>
        <span>Need a hotel for 20 travellers?</span>
        <img src="img/confirm-ic.png" alt="">
    </div>

    <div class="head-page-intro">
        <h1>Hotel Barcelona (and vicinity)</h1>
    </div>

    <div class="head-page-intro">
        <div class="uk-grid uk-grid-small uk-flex-middle" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <ul class="uk-subnav uk-subnav-line hotel-subnav">
                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-calendar.png" alt=""> 18 Apr 2015 – 20 Apr 2015</li>
                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-star-blue.png" alt=""> 2 nights</li>
                </ul>
            </div>
            <div class="uk-width-1-1">
                <ul class="uk-subnav uk-subnav-line hotel-subnav">
                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-bed.png" alt=""> 20 x rooms</li>
                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-men.png" alt=""> x 40</li>
                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-child.png" alt=""> x 0</li>
                </ul>
            </div>
            <div class="uk-width-1-1">
                <a href="#" id="trigger-change-hotel-box" class="uk-button-link change-hotel-search" data-uk-toggle="{target:'#box-change-hotel'}">Change hotel search <i class="uk-icon-angle-right"></i></a>
            </div>
        </div>
    </div>

    <div id="box-change-hotel" class="toggle-change-hotel uk-hidden">
        <ul class="uk-list">
            <li>FC Barcelona vs. Real Madrid CF</li>
            <li>Provisional date | 30 / 31 Oct 2015 or 1 / 2 Nov 2015 <img src="img/confirm-ic.png" alt=""></li>
        </ul>
        <div class="uk-text-center">
            <a href="#" class="uk-button uk-button-primary btn-book">Book tickets only <i class="uk-icon-angle-right"></i></a>
        </div>
    </div>

    <div class="filter-select-page">
        <div class="uk-grid uk-flex-middle">
            <div class="uk-width-1-3 uk-width-small-1-2">
                <a href="#" class="uk-button btn-filter" data-uk-modal="{target: '#modal-hotel-filter', center: true}">Fillter</a>
            </div>
            <div class="uk-width-2-3 uk-width-small-1-2 uk-text-right">
                <a href="#" class="uk-button-link change-hotel-search"><i class="uk-icon-map-marker"></i> Explore on map</a>
            </div>
        </div>

        <div class="uk-grid">
            <div class="uk-width-2-4">
                <div class="uk-button uk-form-select uk-width-1-1 dropdown-clubs" data-uk-form-select>
                    <span class="val-select"></span>
                    <i class="uk-icon-chevron-down"></i>
                    <select>
                        <option value="">Sort by</option>
                        <option value="">Name</option>
                        <option value="">Date</option>
                        <option value="">Bla Bla</option>
                    </select>
                </div>
            </div>
            <div class="uk-width-2-4">
                <div class="uk-button uk-form-select uk-width-1-1 dropdown-clubs" data-uk-form-select>
                    <span class="val-select"></span>
                    <i class="uk-icon-chevron-down"></i>
                    <select>
                        <option value="">Select all hotel</option>
                        <option value="">Mon</option>
                        <option value="">Tue</option>
                        <option value="">Wed</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-list-fixture">
        <div class="avaiable-fixture">
            <div class="uk-grid">
                <div class="uk-width-1-2">
                    63 available hotels
                </div>
                <div class="uk-width-1-2 uk-text-right">
                    Prices: <span class="currency">£ GBP</span>
                </div>
            </div>
        </div>

        <div class="list-hotel-search">
            <div class="hotel-search-item">
                <div class="uk-grid uk-grid-small box-info-hotel" data-uk-margin="" data-uk-grid-match="">
                    <div class="uk-width-small-7-10">
                        <div class="box-name">
                            <h4>Ibis Barcelona Mollet</h4>
                            <div class="score" data-score="4"></div>
                            <span class="truncate-inline">City centre of Mollet del Valles</span>
                        </div>
                        <div class="uk-grid uk-grid-small" data-uk-grid-margin>
                            <div class="uk-width-small-1-2 uk-text-center">
                                <img src="img/8a-hotel-search/hotel-item.jpg" alt="">
                            </div>
                            <div class="uk-width-small-1-2">
                                <ul class="uk-list list-intro">
                                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-check.png" alt=""> Price for <span>2 nights</span></li>
                                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-check.png" alt=""> 20 x rooms</li>
                                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-check.png" alt=""> Breakfast</li>
                                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-check.png" alt=""> Free cancellation</li>
                                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-check.png" alt=""> Hotel taxes & fees</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-small-2-10 uk-flex uk-flex-middle has-space-dot">
                        <div class="from-price">
                            <span>from:</span>
                            <span>£1,000</span>
                        </div>
                    </div>
                    <div class="uk-width-small-1-10 uk-flex uk-flex-middle uk-flex-center">
                        <div class="link-caret">
                            <a href="#"><i class="uk-icon-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hotel-search-item">
                <div class="uk-grid uk-grid-small box-info-hotel" data-uk-margin="" data-uk-grid-match="">
                    <div class="uk-width-small-7-10">
                        <div class="box-name">
                            <h4>Ibis Barcelona Mollet</h4>
                            <div class="score" data-score="4"></div>
                            <span class="truncate-inline">City centre of Mollet del Valles</span>
                        </div>
                        <div class="uk-grid uk-grid-small" data-uk-grid-margin>
                            <div class="uk-width-small-1-2 uk-text-center">
                                <img src="img/8a-hotel-search/hotel-item.jpg" alt="">
                            </div>
                            <div class="uk-width-small-1-2">
                                <ul class="uk-list list-intro">
                                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-check.png" alt=""> Price for <span>2 nights</span></li>
                                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-check.png" alt=""> 20 x rooms</li>
                                    <li class="uk-flex uk-flex-middle"><img src="img/9a-hotel-profile/faile-check.png" alt=""> No breakfast</li>
                                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-check.png" alt=""> Free cancellation</li>
                                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-check.png" alt=""> Hotel taxes & fees</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-small-2-10 uk-flex uk-flex-middle has-space-dot">
                        <div class="from-price">
                            <span>from:</span>
                            <span>£1,000</span>
                        </div>
                    </div>
                    <div class="uk-width-small-1-10 uk-flex uk-flex-middle uk-flex-center">
                        <div class="link-caret">
                            <a href="#"><i class="uk-icon-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hotel-search-item">
                <div class="uk-grid uk-grid-small box-info-hotel" data-uk-margin="" data-uk-grid-match="">
                    <div class="uk-width-small-7-10">
                        <div class="box-name">
                            <h4>Ibis Barcelona Mollet</h4>
                            <div class="score" data-score="4"></div>
                            <span class="truncate-inline">City centre of Mollet del Valles</span>
                        </div>
                        <div class="uk-grid uk-grid-small" data-uk-grid-margin>
                            <div class="uk-width-small-1-2 uk-text-center">
                                <img src="img/8a-hotel-search/hotel-item.jpg" alt="">
                            </div>
                            <div class="uk-width-small-1-2">
                                <ul class="uk-list list-intro">
                                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-check.png" alt=""> Price for <span>2 nights</span></li>
                                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-check.png" alt=""> 20 x rooms</li>
                                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-check.png" alt=""> Breakfast</li>
                                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-check.png" alt=""> Free cancellation</li>
                                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-check.png" alt=""> Hotel taxes & fees</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-small-2-10 uk-flex uk-flex-middle has-space-dot">
                        <div class="from-price">
                            <span>from:</span>
                            <span>£1,000</span>
                        </div>
                    </div>
                    <div class="uk-width-small-1-10 uk-flex uk-flex-middle uk-flex-center">
                        <div class="link-caret">
                            <a href="#"><i class="uk-icon-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-grid wrap-showmore uk-margin-top">
            <div class="uk-width-1-1">
                <div class="uk-text-center">
                    <a id="show-more" href="#" class="show-more"><b>More hotel options</b></a>
                </div>
            </div>
        </div>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>

    <div id="modal-hotel-filter" class="uk-modal">
        <div class="uk-modal-dialog">
            <a href="" class="uk-modal-close uk-close"></a>
            <div class="filter-head">
                <h4 class="uk-margin-remove">Filter results</h4>
                <p class="uk-margin-remove">329 hotel results</p>
                <a href="#" class="reset">Reset all filters x</a>
            </div>
            <div class="filter-field-row">
                <form class="uk-form uk-form-stacked common-form-modal">
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="form-input-1">Hotel name contains</label>
                        <div class="uk-form-controls">
                            <input type="text" id="form-input-1" class="common-modal-ipt" placeholder="Hotel name...">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <div class="uk-form-row">
                            <label class="uk-form-label common-modal-label" for="">Hotel name</label>
                            <div class="uk-grid">
                                <div class="uk-width-3-4">
                                    <div class="uk-form-controls">
                                        <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                            <span></span>
                                            <i class="uk-icon-chevron-down"></i>
                                            <select>
                                                <option value="">All hotel</option>
                                                <option value="">Hotel 1</option>
                                                <option value="">Hotel 2</option>
                                                <option value="">Hotel 3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="form-input-1">Price (total stay)</label>
                        <div class="uk-grid">
                            <div class="uk-width-1-2 uk-width-small-1-4">
                                <span>Min (£)</span>
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">0</option>
                                            <option value="">1</option>
                                            <option value="">2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2 uk-width-small-1-4">
                                <span>Max (£)</span>
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">500+</option>
                                            <option value="">400+</option>
                                            <option value="">300+</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="form-input-1">Hotel star rating contains</label>
                        <div class="uk-form-controls">
                            <div class="filter-rate"></div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <div class="uk-form-row">
                            <label class="uk-form-label common-modal-label" for="">Area locations</label>
                            <div class="uk-grid">
                                <div class="uk-width-3-4">
                                    <div class="uk-form-controls">
                                        <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                            <span></span>
                                            <i class="uk-icon-chevron-down"></i>
                                            <select>
                                                <option value="">All area locations</option>
                                                <option value="">locations 1</option>
                                                <option value="">locations 2</option>
                                                <option value="">locations 3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Airpots</label>
                        <div class="uk-grid uk-flex-middle">
                            <div class="uk-width-1-2">
                                <span>BCN</span>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Train station</label>
                        <div class="uk-grid uk-flex-middle">
                            <div class="uk-width-1-2">
                                <span>Estacio de Franca</span>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Tube stations</label>
                        <div class="uk-grid uk-flex-middle">
                            <div class="uk-width-1-2">
                                <span>Barceloneta</span>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Bus stations</label>
                        <div class="uk-grid uk-flex-middle">
                            <div class="uk-width-1-2">
                                <span>Barcela Nord</span>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Place of interest</label>
                        <div class="uk-grid uk-flex-middle">
                            <div class="uk-width-1-2">
                                <span>Museo Picasso</span>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Shopping areas</label>
                        <div class="uk-grid uk-flex-middle">
                            <div class="uk-width-1-2">
                                <span>Maremagnum</span>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label common-modal-label" for="">Exhibition centres</label>
                        <div class="uk-grid uk-flex-middle">
                            <div class="uk-width-1-2">
                                <span>Fifa de Barcelona</span>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="uk-form-controls">
                                    <div class="uk-button uk-form-select uk-width-1-1 common-dropdown" data-uk-form-select="">
                                        <span></span>
                                        <i class="uk-icon-chevron-down"></i>
                                        <select>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                            <option value="">0.5km</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>

<script type="text/javascript">
    $(document).ready(function(){
        $(".score").raty({
            number: 5,
            numberMax: 5,
            starOn: 'img/8a-hotel-search/8a-star-on.png',
            starOff: 'img/8a-hotel-search/8a-star-off.png',
            readOnly: true,
            score: function(){
                return $(this).attr('data-score')
            }
        });

        $(".filter-rate").raty({
            number: 5,
            numberMax: 5,
            starOn: 'img/8a-hotel-search/8a-star-on.png',
            starOff: 'img/8a-hotel-search/8a-star-off.png'
        });

        $("#trigger-change-hotel-box").on('click', function(e){
            e.preventDefault();
            var inner = $("#box-change-hotel").is(':visible') ? 'Change hotel search <i class="uk-icon-angle-right"></i>' : 'Change hotel search <i class="uk-icon-angle-down"></i>';
            $(this).html(inner);
        });
    });
</script>
