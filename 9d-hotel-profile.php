<?php include "include/header.php" ?>

<section id="content">
    <div class="head-page-intro">
        <h1>Hotel Ibis Barcelona Mollet</h1>
        <div class="score" data-score="4"></div>
        <p>C/ Nicaragua s/n, Mollet Del Vallas, 8100, Spain</p>
        <ul class="uk-subnav uk-subnav-line hotel-subnav air-map">
            <li class="uk-flex uk-flex-middle">Airport of Viladecans</li>
            <li class="uk-flex uk-flex-middle"><img src="img/9a-hotel-profile/9a-maker.png" alt=""><a href="#">Map</a></li>
        </ul>
    </div>

    <div class="block-info-hotel">
        <div class="list-hotel-action">
            <div data-uk-slider="{infinite: false}">
                <div class="uk-slider-container">
                    <ul class="uk-slider uk-grid uk-grid-divider uk-grid-width-1-3 uk-grid-width-small-1-5">
                        <li class="uk-text-center">
                            <a href="#">TripAdvisor<br>reviews</a>
                        </li>
                        <li class="uk-text-center">
                            <a href="#">Choose<br>your rooms</a>
                        </li>
                        <li class="uk-text-center">
                            <a href="#">Hotel<br>information</a>
                        </li>
                        <li class="uk-text-center">
                            <a href="#">Policy &<br>conditions</a>
                        </li>
                        <li class="uk-text-center">
                            <a href="#">Extra<br>fees</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="uk-grid uk-flex-middle" data-uk-slideshow="{animation: 'scroll'}">
            <div class="uk-width-2-10 uk-text-left">
                <a href="" data-uk-slideshow-item="previous"><img src="img/9a-hotel-profile/prev.png" alt=""></a>
            </div>
            <div class="uk-width-6-10">
                <ul class="uk-slideshow" >
                    <li><img src="img/9a-hotel-profile/hotel-slide-01.jpg" width="" height="" alt=""></li>
                    <li><img src="img/9a-hotel-profile/hotel-slide-01.jpg" width="" height="" alt=""></li>
                    <li><img src="img/9a-hotel-profile/hotel-slide-01.jpg" width="" height="" alt=""></li>
                </ul>
            </div>
            <div class="uk-width-2-10 uk-text-right">
                <a href="" data-uk-slideshow-item="next"><img src="img/9a-hotel-profile/next.png" alt=""></a>
            </div>
        </div>
    </div>

    <div class="head-page-intro">
        <ul class="uk-subnav uk-subnav-line hotel-subnav">
            <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-calendar.png" alt=""> 18 Apr 2015 – 20 Apr 2015</li>
            <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-star-blue.png" alt=""> 2 nights</li>
            <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-bed.png" alt=""> 20 x rooms</li>
        </ul>
        <div class="uk-grid uk-grid-collapse uk-flex-middle">
            <div class="uk-width-1-2">
                <ul class="uk-subnav uk-subnav-line hotel-subnav">
                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-men.png" alt=""> x 40</li>
                    <li class="uk-flex uk-flex-middle"><img src="img/8a-hotel-search/8a-child.png" alt=""> x 0</li>
                </ul>
            </div>
            <div class="uk-width-1-2 uk-text-right">
                <a href="#" class="uk-button-link change-hotel-search">Change hotel search <i class="uk-icon-angle-right"></i></a>
            </div>
        </div>
    </div>

    <div class="wrap-list-fixture">
        <div class="avaiable-fixture">
            <div class="uk-grid uk-grid-collapse">
                <div class="uk-width-2-3">
                    Select your room options
                </div>
                <div class="uk-width-1-3 uk-text-right">
                    Prices: <span class="currency">£ GBP</span>
                </div>
            </div>
        </div>

        <div class="list-hotel-search">
            <div class="room-option-item">
                <div class="uk-grid uk-grid-collapse" data-uk-grid-match>
                    <div class="uk-width-small-6-10">
                        <ul class="uk-list list-room-people">
                            <li class="uk-grid uk-grid-small">
                                <div class="uk-width-2-10">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                </div>
                                <div class="uk-width-8-10">
                                    <span>Triple room x 3</span>
                                </div>
                            </li>
                            <li class="uk-grid uk-grid-small">
                                <div class="uk-width-2-10">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                </div>
                                <div class="uk-width-8-10">
                                    <span>Twin room x 19</span>
                                </div>
                            </li>
                            <li class="uk-grid uk-grid-small">
                                <div class="uk-width-2-10">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                </div>
                                <div class="uk-width-8-10">
                                    <span>Single room x 1</span>
                                </div>
                            </li>
                            <li class="uk-grid uk-grid-small">
                                <div class="uk-width-1-1">
                                    <span class="config">Bed configuration</span> <img src="img/confirm-ic.png" alt="">
                                </div>
                            </li>
                        </ul>
                        <ul class="uk-list list-room-check">
                            <li class="uk-flex uk-flex-middle">
                                <img src="img/8a-hotel-search/8a-check.png" alt="">
                                Buffet breakfast (continental)
                            </li>
                            <li class="uk-flex uk-flex-middle">
                                <img src="img/8a-hotel-search/8a-check.png" alt="">
                                Free cancellation <span>until Nov 20, 2015</span>
                            </li>
                            <li class="uk-flex uk-flex-middle">
                                <img src="img/8a-hotel-search/8a-check.png" alt="">
                                Hotel taxes & service fees
                            </li>
                            <li class="uk-flex uk-flex-middle">
                                <img src="img/9a-hotel-profile/faile-check.png" alt="">
                                City tourist taxes
                            </li>
                        </ul>
                    </div>
                    <div class="uk-width-small-4-10 uk-position-relative">
                        <div class="room-option-price">
                            <span>£1,000</span>
                            <p>Total price for <b>2 nights:</b></p>
                            <a href="#" class="uk-button uk-button-primary btn-book-now">Submit enquiry <i class="uk-icon-angle-right"></i></a>
                        </div>
                        <div class="ticket-only">
                            <a href="#">Book tickets only  <i class="uk-icon-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="room-option-item">
                <div class="uk-grid uk-grid-collapse" data-uk-grid-match>
                    <div class="uk-width-small-6-10">
                        <ul class="uk-list list-room-people">
                            <li class="uk-grid uk-grid-small">
                                <div class="uk-width-2-10">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                </div>
                                <div class="uk-width-8-10">
                                    <span>Triple room x 3</span>
                                </div>
                            </li>
                            <li class="uk-grid uk-grid-small">
                                <div class="uk-width-2-10">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                </div>
                                <div class="uk-width-8-10">
                                    <span>Twin room x 19</span>
                                </div>
                            </li>
                            <li class="uk-grid uk-grid-small">
                                <div class="uk-width-2-10">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                </div>
                                <div class="uk-width-8-10">
                                    <span>Single room x 1</span>
                                </div>
                            </li>
                            <li class="uk-grid uk-grid-small">
                                <div class="uk-width-1-1">
                                    <span class="config">Bed configuration</span> <img src="img/confirm-ic.png" alt="">
                                </div>
                            </li>
                        </ul>
                        <ul class="uk-list list-room-check">
                            <li class="uk-flex uk-flex-middle">
                                <img src="img/8a-hotel-search/8a-check.png" alt="">
                                Buffet breakfast (continental)
                            </li>
                            <li class="uk-flex uk-flex-middle">
                                <img src="img/8a-hotel-search/8a-check.png" alt="">
                                Free cancellation <span>until Nov 20, 2015</span>
                            </li>
                            <li class="uk-flex uk-flex-middle">
                                <img src="img/8a-hotel-search/8a-check.png" alt="">
                                Hotel taxes & service fees
                            </li>
                            <li class="uk-flex uk-flex-middle">
                                <img src="img/9a-hotel-profile/faile-check.png" alt="">
                                City tourist taxes
                            </li>
                        </ul>
                    </div>
                    <div class="uk-width-small-4-10 uk-position-relative">
                        <div class="room-option-price">
                            <span>£1,000</span>
                            <p>Total price for <b>2 nights:</b></p>
                            <a href="#" class="uk-button uk-button-primary btn-book-now">Submit enquiry <i class="uk-icon-angle-right"></i></a>
                        </div>
                        <div class="ticket-only">
                            <a href="#">Book tickets only  <i class="uk-icon-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="room-option-item">
                <div class="uk-grid uk-grid-collapse" data-uk-grid-match>
                    <div class="uk-width-small-6-10">
                        <ul class="uk-list list-room-people">
                            <li class="uk-grid uk-grid-small">
                                <div class="uk-width-2-10">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                </div>
                                <div class="uk-width-8-10">
                                    <span>Triple room x 3</span>
                                </div>
                            </li>
                            <li class="uk-grid uk-grid-small">
                                <div class="uk-width-2-10">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                </div>
                                <div class="uk-width-8-10">
                                    <span>Twin room x 19</span>
                                </div>
                            </li>
                            <li class="uk-grid uk-grid-small">
                                <div class="uk-width-2-10">
                                    <img src="img/9a-hotel-profile/people-room-on.png" alt="">
                                </div>
                                <div class="uk-width-8-10">
                                    <span>Single room x 1</span>
                                </div>
                            </li>
                            <li class="uk-grid uk-grid-small">
                                <div class="uk-width-1-1">
                                    <span class="config">Bed configuration</span> <img src="img/confirm-ic.png" alt="">
                                </div>
                            </li>
                        </ul>
                        <ul class="uk-list list-room-check">
                            <li class="uk-flex uk-flex-middle">
                                <img src="img/8a-hotel-search/8a-check.png" alt="">
                                Buffet breakfast (continental)
                            </li>
                            <li class="uk-flex uk-flex-middle">
                                <img src="img/8a-hotel-search/8a-check.png" alt="">
                                Free cancellation <span>until Nov 20, 2015</span>
                            </li>
                            <li class="uk-flex uk-flex-middle">
                                <img src="img/8a-hotel-search/8a-check.png" alt="">
                                Hotel taxes & service fees
                            </li>
                            <li class="uk-flex uk-flex-middle">
                                <img src="img/9a-hotel-profile/faile-check.png" alt="">
                                City tourist taxes
                            </li>
                        </ul>
                    </div>
                    <div class="uk-width-small-4-10 uk-position-relative">
                        <div class="room-option-price">
                            <span>£1,000</span>
                            <p>Total price for <b>2 nights:</b></p>
                            <a href="#" class="uk-button uk-button-primary btn-book-now">Submit enquiry <i class="uk-icon-angle-right"></i></a>
                        </div>
                        <div class="ticket-only">
                            <a href="#">Book tickets only  <i class="uk-icon-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-grid wrap-showmore uk-margin-top">
            <div class="uk-width-1-1">
                <div class="uk-text-center">
                    <a id="show-more" href="#" class="show-more"><b>Show 5 more room options…</b></a>
                </div>
            </div>
        </div>
    </div>

    <div class="other-list-review">
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-hotel-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Hotel description</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-map-maker-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Hotel location</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-google-map-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Google map</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-H-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Hotel facilities</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-room-faci-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Room facilities</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-hotel-poli-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Hotel policies & conditions</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-extra-card-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">Extra fees & taxes payable by guests</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
        <a href="#" class="uk-grid uk-grid-small uk-flex-middle other-list-item">
            <div class="uk-width-1-10 uk-text-center">
                <img src="img/9a-hotel-profile/9a-tripad-icon.png" alt="">
            </div>
            <div class="uk-width-8-10">
                <h4 class="truncate">TripAdvisor reviews</h4>
            </div>
            <div class="uk-width-1-10 uk-text-center">
                <i class="uk-icon-chevron-right"></i>
            </div>
        </a>
    </div>

    <div class="list-follow">
        <h1>follow us</h1>
        <div class="uk-text-center">
            <ul class="uk-subnav">
                <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
                <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
                <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>

<script type="text/javascript">
    $(document).ready(function(){
        $(".score").raty({
            number: 5,
            numberMax: 5,
            starOn: 'img/8a-hotel-search/8a-star-on.png',
            starOff: 'img/8a-hotel-search/8a-star-off.png',
            readOnly: true,
            score: function(){
                return $(this).attr('data-score')
            }
        });
    });
</script>
