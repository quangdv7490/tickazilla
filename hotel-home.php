<?php include "include/header.php" ?>

<section id="content">
    <div class="banner hotel-banner">
        <figure class="uk-overlay" href="">
            <img src="img/hotel-item/hotel-bg-banner.jpg" alt="">
            <figcaption class="uk-overlay-panel uk-flex uk-flex-middle">
                <div class="search-hotel">
                    <form action="" class="uk-form uk-form-stacked">
                        <div class="uk-form-row">
                            <label class="uk-form-label label-plan truncate">
                                <img src="img/camera-icon.png" alt="icon">
                                Plan & book your perfect holiday!
                            </label>
                            <div class="uk-form-icon uk-width-1-1">
                                <i class="uk-icon-map-marker map-maker"></i>
                                <input class="hotel-search" type="text" name="home-search" placeholder="City, country, hotel name…">
                            </div>
                        </div>

                        <div class="uk-form-row uk-grid">
                            <div class="uk-width-1-2">
                                <label for="" class="uk-form-label label-checkin truncate">Check-in</label>
                                <div class="uk-form-icon uk-width-1-1">
                                    <i class="uk-icon-calendar calendar-ic"></i>
                                    <input type="text" name="" id="check-in-hotel" class="ipt-checkin uk-width-1-1" >
                                </div>
                            </div>
                            <div class="uk-width-1-2">
                                <label for="" class="uk-form-label label-checkin truncate">Check-out</label>
                                <div class="uk-form-icon uk-width-1-1">
                                    <i class="uk-icon-calendar calendar-ic"></i>
                                    <input type="text" name="" id="hotel-check-out" class="ipt-checkin uk-width-1-1">
                                </div>
                            </div>
                        </div>

                        <div class="uk-form-row uk-grid">
                            <div class="uk-width-1-3">
                                <label for="" class="uk-form-label label-cm truncate">Nights</label>
                                <div class="uk-button drop-night-room uk-form-select uk-width-1-1" data-uk-form-select>
                                    <span></span>
                                    <i class="uk-icon-chevron-down"></i>
                                    <select>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <label for="" class="uk-form-label label-cm truncate">Rooms</label>
                                <div class="uk-button drop-night-room uk-form-select uk-width-1-1" data-uk-form-select>
                                    <span></span>
                                    <i class="uk-icon-chevron-down"></i>
                                    <select>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="uk-form-row uk-grid uk-flex-middle">
                            <div class="uk-width-1-5">
                                <label class="uk-form-label label-cm">Room 1:</label>
                            </div>
                            <div class="uk-width-2-5">
                                <label for="" class="uk-form-label label-cm truncate">Adults (13+)</label>
                                <div class="uk-button drop-select-room uk-form-select uk-width-1-1" data-uk-form-select>
                                    <img class="adult" src="img/adult.png" alt="">
                                    <span></span>
                                    <i class="uk-icon-chevron-down"></i>
                                    <select>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-2-5">
                                <label for="" class="uk-form-label label-cm truncate">Children (0-12)</label>
                                <div class="uk-button drop-select-room uk-form-select uk-width-1-1" data-uk-form-select>
                                    <img class="children" src="img/child.png" alt="">
                                    <span></span>
                                    <i class="uk-icon-chevron-down"></i>
                                    <select>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="uk-form-row uk-grid uk-flex-middle">
                            <div class="uk-width-1-5">
                                <label class="uk-form-label label-cm">Room 2:</label>
                            </div>
                            <div class="uk-width-2-5">
                                <label for="" class="uk-form-label label-cm truncate">Adults (13+)</label>
                                <div class="uk-button drop-select-room uk-form-select uk-width-1-1" data-uk-form-select>
                                    <img class="adult" src="img/adult.png" alt="">
                                    <span></span>
                                    <i class="uk-icon-chevron-down"></i>
                                    <select>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-2-5">
                                <label for="" class="uk-form-label label-cm truncate">Children (0-12)</label>
                                <div class="uk-button drop-select-room uk-form-select uk-width-1-1" data-uk-form-select>
                                    <img class="children" src="img/child.png" alt="">
                                    <span></span>
                                    <i class="uk-icon-chevron-down"></i>
                                    <select>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="uk-form-row">
                            <button class="hotel-submit"><i class="uk-icon-search"></i> Search</button>
                        </div>
                    </form>
                </div>
            </figcaption>
        </figure>
    </div>

    <div class="list-event-home">
        <div class="uk-container uk-container-center">
            <div id="wrap-list-event" class="uk-grid uk-grid-small" data-uk-grid-match="{target: '.event-home-item'}">
                <div class="uk-width-1-2">
                    <div class="event-home-item">
                        <div class="retangle">
                            <div class="inner">
                                <a href="#">
                                    <img src="img/hotel-item/hotel-item-01.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="info-name">
                            <a href="#">Mandrid</a>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2">
                    <div class="event-home-item">
                        <div class="retangle">
                            <div class="inner">
                                <a href="#">
                                    <img src="img/hotel-item/hotel-item-02.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="info-name">
                            <a href="#">Barcelona</a>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2">
                    <div class="event-home-item">
                        <div class="retangle">
                            <div class="inner">
                                <a href="#">
                                    <img src="img/hotel-item/hotel-item-03.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="info-name">
                            <a href="#">Munich</a>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2">
                    <div class="event-home-item">
                        <div class="retangle">
                            <div class="inner">
                                <a href="#">
                                    <img src="img/hotel-item/hotel-item-04.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="info-name">
                            <a href="#">Milan</a>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2">
                    <div class="event-home-item">
                        <div class="retangle">
                            <div class="inner">
                                <a href="#">
                                    <img src="img/hotel-item/hotel-item-05.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="info-name">
                            <a href="#">Rome</a>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2">
                    <div class="event-home-item">
                        <div class="retangle">
                            <div class="inner">
                                <a href="#">
                                    <img src="img/hotel-item/hotel-item-06.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="info-name">
                            <a href="#">Amsterdam</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="uk-grid wrap-showmore">
                <div class="uk-width-1-1">
                    <div class="uk-text-center">
                        <a id="show-more" href="#" class="show-more"><b>show 10 more popular destinations…</b></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>

<script>
    $(document).ready(function(){
        var item = '<div class="uk-width-1-2">';
        item += '<div class="event-home-item">';
        item += '<div class="retangle">';
        item += '<div class="inner">';
        item += '<a href="#">';
        item += '<img src="img/hotel-item/hotel-item-01.png" alt="">';
        item += '</a>';
        item += '</div>';
        item += '</div>';
        item += '<div class="info-name">';
        item += '<a href="#">Mandrid</a>';
        item += '</div>';
        item += '</div>';
        item += '</div>';

        $("#show-more").on('click', function(e){
            e.preventDefault();
            for (var i = 1; i <= 10; i++)
            {
                $("#wrap-list-event").append(item);
            }
        });

        $(function(){
            $("#check-in-hotel").datepicker({
                position: 'bottom left',
                language: 'en',
                autoClose: true
            });

            $("#hotel-check-out").datepicker({
                position: 'bottom right',
                language: 'en',
                autoClose: true
            });
        })

    });
</script>

