<div class="list-follow">
   <h1>Follow us</h1>

   <div class="uk-text-center">
      <ul class="uk-subnav">
         <li><a href="#" class="social-face"><i class="uk-icon-facebook-f"></i></a></li>
         <li><a href="#" class="social-twitt"><i class="uk-icon-twitter"></i></a></li>
         <li><a href="#" class="social-link"><i class="uk-icon-linkedin"></i></a></li>
      </ul>
   </div>
</div>