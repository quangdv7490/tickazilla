<footer class="footer">
    <div class="uk-text-center">
        <ul class="uk-subnav">
            <li><a href="#">Cookie Policy</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Customer Support</a></li>
        </ul>
        <p>© 2002-2016 Football Encounters UK Ltd</p>
    </div>
</footer>

<script src="js/jquery/jquery-2.2.1.min.js"></script>
<script src="js/uikit.min.js"></script>
<script src="js/components/sticky.min.js"></script>
<script src="js/components/form-select.min.js"></script>
<script src="js/components/slideshow.min.js"></script>
<script src="js/components/slider.min.js"></script>
<script src="js/components/accordion.min.js"></script>
<script src="js/datepicker/datepicker.js"></script>
<script src="js/datepicker/i18n/datepicker.en.js"></script>
<script src="js/raty/lib/jquery.raty.js"></script>
<script src="js/shorten/jquery.shorten.1.0.js"></script>

<script src="js/typeahead.jquery.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjIXKQwgUfXbF_e7jqLkbNuAoSSdCtTDI&sensor=true" type="text/javascript"></script>
<script src="js/infobox.js"></script>
<script src="js/gmaps.js"></script>

<script>
    var num = Math.random();
    $(function() {
        $('img').each(function(i,o){
            var src = $(o).attr('src') + '?v='+num;
            $(o).attr('src',src);
        })
    })
</script>

</body>
</html>