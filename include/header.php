<?php 
	$v = time();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0"/>

    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
  
    <link rel="stylesheet" href="css/uikit.gradient.min.css">
    <link rel="stylesheet" href="css/components/sticky.gradient.min.css">
    <link rel="stylesheet" href="css/components/form-select.gradient.min.css">
    <link rel="stylesheet" href="css/components/form-advanced.gradient.min.css">
    <link rel="stylesheet" href="css/components/slideshow.gradient.min.css">
    <link rel="stylesheet" href="css/components/slider.gradient.min.css">
    <link rel="stylesheet" href="css/components/accordion.gradient.min.css">
    <link rel="stylesheet" href="css/datepicker/datepicker-theme.css">
    <link rel="stylesheet" href="css/custom/custom-style.css?t=<?php echo $v;?>">
    <link rel="stylesheet" href="css/custom/custom-maker.css">
    <link rel="stylesheet" href="css/custom/custom-other.css?ts=<?php echo $v+1;?>">
</head>
<body>
<nav class="uk-navbar tickazilla-navbar" data-uk-sticky="">
    <ul class="uk-navbar-nav uk-navbar-flip">
        <li><a href="#"><img src="img/sign-in-icon.png" alt=""></a></li>
        <li><a href="#"><img src="img/shopping-card-icon.png" alt=""></a></li>
    </ul>
    <a href="#" class="uk-navbar-toggle" data-uk-offcanvas="{target: '#canvas-menu'}"></a>
    <a href="#" class="uk-navbar-brand uk-navbar-center brand-mobile">
        <img src="img/tickazilla-logo.png" alt="logo">
    </a>
</nav>