<!-- Start canvas-menu -->
<div id="canvas-menu" class="uk-offcanvas my-offcanvas-menu">
    <div class="uk-offcanvas-bar">
        <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="">
            <li>
                <div class="uk-grid uk-grid-small uk-flex-middle menu-list-item">
                    <div class="uk-width-1-6">
                        <img src="img/icon-menu/shape-1.png" alt="">
                    </div>
                    <div class="uk-width-5-6">
                        <a href="#" class="truncate">Home</a>
                    </div>
                </div>
            </li>
            <li class="uk-nav-divider"></li>
            <li>
                <div class="uk-grid uk-grid-small uk-flex-middle menu-list-item">
                    <div class="uk-width-1-6">
                        <img src="img/icon-menu/shape-2.png" alt="">
                    </div>
                    <div class="uk-width-5-6">
                        <a href="#" class="truncate">Football</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-grid uk-grid-small uk-flex-middle menu-list-item">
                    <div class="uk-width-1-6">
                        <img src="img/icon-menu/shape-3.png" alt="">
                    </div>
                    <div class="uk-width-5-6">
                        <a href="#" class="truncate">Rugby</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-grid uk-grid-small uk-flex-middle menu-list-item">
                    <div class="uk-width-1-6">
                        <img src="img/icon-menu/shape-4.png" alt="">
                    </div>
                    <div class="uk-width-5-6">
                        <a href="#" class="truncate">Basketball</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-grid uk-grid-small uk-flex-middle menu-list-item">
                    <div class="uk-width-1-6">
                        <img src="img/icon-menu/shape-5.png" alt="">
                    </div>
                    <div class="uk-width-5-6">
                        <a href="#" class="Munich Oktoberfest">Munich Oktoberfest</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-grid uk-grid-small uk-flex-middle menu-list-item">
                    <div class="uk-width-1-6">
                        <img src="img/icon-menu/shape-6.png" alt="">
                    </div>
                    <div class="uk-width-5-6">
                        <a href="#" class="truncate">Hotels</a>
                    </div>
                </div>
            </li>
            <li class="uk-nav-divider"></li>
            <li>
                <div class="uk-grid uk-grid-small uk-flex-middle menu-list-item">
                    <div class="uk-width-1-6">
                        <img src="img/icon-menu/shape-7.png" alt="">
                    </div>
                    <div class="uk-width-5-6">
                        <a href="#" class="truncate">Your account</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-grid uk-grid-small uk-flex-middle menu-list-item">
                    <div class="uk-width-1-6">
                        <img src="img/icon-menu/shape-8.png" alt="">
                    </div>
                    <div class="uk-width-5-6">
                        <a href="#" class="truncate">Customer reviews</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-grid uk-grid-small uk-flex-middle menu-list-item">
                    <div class="uk-width-1-6">
                        <img src="img/icon-menu/shape-9.png" alt="">
                    </div>
                    <div class="uk-width-5-6">
                        <a href="#" class="truncate">Customer support</a>
                    </div>
                </div>
            </li>
            <li class="uk-nav-divider"></li>
            <li>
                <div class="uk-grid uk-grid-small uk-flex-middle menu-list-item">
                    <div class="uk-width-1-6">
                        <img src="img/icon-menu/shape-10.png" alt="">
                    </div>
                    <div class="uk-width-5-6">
                        <a href="#" class="truncate">About us</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-grid uk-grid-small uk-flex-middle menu-list-item">
                    <div class="uk-width-1-6">
                        <img src="img/icon-menu/shape-11.png" alt="">
                    </div>
                    <div class="uk-width-5-6">
                        <a href="#" class="truncate">Privacy policy</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-grid uk-grid-small uk-flex-middle menu-list-item">
                    <div class="uk-width-1-6">
                        <img src="img/icon-menu/shape-12.png" alt="">
                    </div>
                    <div class="uk-width-5-6">
                        <a href="#" class="truncate">Cookie policy</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-grid uk-grid-small uk-flex-middle menu-list-item">
                    <div class="uk-width-1-6">
                        <img src="img/icon-menu/shape-13.png" alt="">
                    </div>
                    <div class="uk-width-5-6">
                        <a href="#" class="truncate">Tickazilla desktop site</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- End canvas-menu -->