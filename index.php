<?php include "include/header.php" ?>

<section id="content">
   <div class="banner">
      <figure class="uk-overlay non-over-hidden">
         <img src="img/home-banner.png" alt="">
         <figcaption class="uk-overlay-panel uk-flex uk-flex-middle">
            <div class="search-banner">
               <form action="" class="uk-form uk-form-stacked">
                  <div class="uk-form-row">
                     <label for="home-search" class="uk-form-label truncate">
                        <img src="img/camera-icon.png" alt="icon">
                        Find your perfect holiday!
                     </label>

                     <div class="uk-form-icon uk-width-1-1">
                         <!-- <i class="uk-icon-map-marker"></i> -->
                        <input type="text" class="hotel-search" name="home-search" id="home-search" placeholder="Team, event, city, country…">
                        <a href="#" class="remove-val-textbox"><img src="img/clear.png" alt=""></a>
                     </div>
                  </div>
               </form>
            </div>
         </figcaption>
      </figure>
   </div>

   <div class="list-cate">
      <a href="#" class="uk-grid uk-grid-small uk-flex-middle cate-item">
         <div class="uk-width-1-6">
            <div class="uk-text-center">
               <img class="icon-list-home" src="img/ball.png" alt="ball">
            </div>
         </div>
         <div class="uk-width-4-6">
            <div class="wrap-info">
               <h4 class="truncate">Football (Soccer)</h4>

               <p class="truncate">Book tickets & hotel packages</p>
            </div>
         </div>
         <div class="uk-width-1-6 uk-text-right">
            <i class="uk-icon-chevron-right"></i>
         </div>
      </a>
      <a href="#" class="uk-grid uk-grid-small uk-flex-middle cate-item">
         <div class="uk-width-1-6">
            <div class="uk-text-center">
               <img class="icon-list-home" src="img/ball-2.png" alt="ball">
            </div>
         </div>
         <div class="uk-width-4-6">
            <div class="wrap-info">
               <h4 class="truncate">Rugby</h4>

               <p class="truncate">Book tickets & hotel packages</p>
            </div>
         </div>
         <div class="uk-width-1-6 uk-text-right">
            <i class="uk-icon-chevron-right"></i>
         </div>
      </a>
      <a href="#" class="uk-grid uk-grid-small uk-flex-middle cate-item">
         <div class="uk-width-1-6">
            <div class="uk-text-center">
               <img class="icon-list-home" src="img/ball-3.png" alt="ball">
            </div>
         </div>
         <div class="uk-width-4-6">
            <div class="wrap-info">
               <h4 class="truncate">Basketball</h4>

               <p class="truncate">Book tickets & hotel packages</p>
            </div>
         </div>
         <div class="uk-width-1-6 uk-text-right">
            <i class="uk-icon-chevron-right"></i>
         </div>
      </a>
      <a href="#" class="uk-grid uk-grid-small uk-flex-middle cate-item">
         <div class="uk-width-1-6">
            <div class="uk-text-center">
               <img class="icon-list-home" src="img/beer.png" alt="ball">
            </div>
         </div>
         <div class="uk-width-4-6">
            <div class="wrap-info">
               <h4 class="truncate">Munich Oktoberfest</h4>

               <p class="truncate">Book tickets & hotel packages</p>
            </div>
         </div>
         <div class="uk-width-1-6 uk-text-right">
            <i class="uk-icon-chevron-right"></i>
         </div>
      </a>
      <a href="#" class="uk-grid uk-grid-small uk-flex-middle cate-item">
         <div class="uk-width-1-6">
            <div class="uk-text-center">
               <img class="icon-list-home" src="img/hotel.png" alt="ball">
            </div>
         </div>
         <div class="uk-width-4-6">
            <div class="wrap-info">
               <h4 class="truncate">Hotels</h4>

               <p class="truncate">Find & book hotels</p>
            </div>
         </div>
         <div class="uk-width-1-6 uk-text-right">
            <i class="uk-icon-chevron-right"></i>
         </div>
      </a>
   </div>

   <?php include "include/block-follow.php"; ?>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>

<script>
   $(document).ready(function () {
      $(function () {
         var searchIpt = $("#home-search");
         var clrButton = searchIpt.siblings(".remove-val-textbox");
         searchIpt.on('keyup', function () {
            var valIpt = $(this).val();
            (valIpt.length) ? clrButton.show() : clrButton.hide();
         });

         clrButton.on('click', function (e) {
            e.preventDefault();
            $(this).hide();
            searchIpt.val("");
         });
      });


      //http://twitter.github.io/typeahead.js/examples/
      var substringMatcher = function (strs) {
         return function findMatches(q, cb) {
            var matches, substringRegex;
            // an array that will be populated with substring matches
            matches = [];
            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');
            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function (i, str) {
               if (substrRegex.test(str)) {
                  matches.push(str);
               }
            });
            cb(matches);
         };
      };

      var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
         'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
         'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
         'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
         'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
         'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
         'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
         'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
         'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
      ];

      $('.typeahead').typeahead({
         hint: true,
         highlight: true,
         minLength: 1
      }, {
         name: 'states',
         source: substringMatcher(states)
      });
   });
</script>

