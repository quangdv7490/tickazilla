<?php include "include/header.php" ?>

<section id="content">
    <div class="banner">
        <figure class="uk-overlay" href="">
            <img src="img/home-banner.png" alt="">
            <figcaption class="uk-overlay-panel uk-flex uk-flex-middle">
                <div class="search-banner">
                    <form action="" class="uk-form uk-form-stacked">
                        <div class="uk-form-row">
                            <label for="home-search" class="uk-form-label truncate">
                                <img src="img/camera-icon.png" alt="icon">
                                Plan & book your perfect holiday!
                            </label>
                            <div class="uk-form-icon">
                                <i class="uk-icon-map-marker"></i>
                                <input type="text" name="home-search" id="home-search" placeholder="Team, league, city, country…">
                            </div>
                        </div>
                    </form>
                </div>
            </figcaption>
        </figure>
    </div>
    
    <div class="list-event-home">
        <div class="uk-container uk-container-center">
            <div id="wrap-list-event" class="uk-grid uk-grid-small" data-uk-grid-match="{target: '.event-home-item'}">
                <div class="uk-width-1-2">
                    <div class="event-home-item">
                        <div class="retangle">
                            <div class="inner">
                                <a href="#">
                                    <img src="img/event-item/fc-01.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="info-name">
                            <a href="#">Real Mandrid FC</a>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2">
                    <div class="event-home-item">
                        <div class="retangle">
                            <div class="inner">
                                <a href="#">
                                    <img src="img/event-item/fc-02.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="info-name">
                            <a href="#">Borussia Monchengladbach</a>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2">
                    <div class="event-home-item">
                        <div class="retangle">
                            <div class="inner">
                                <a href="#">
                                    <img src="img/event-item/fc-03.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="info-name">
                            <a href="#">Real Mandrid FC</a>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2">
                    <div class="event-home-item">
                        <div class="retangle">
                            <div class="inner">
                                <a href="#">
                                    <img src="img/event-item/fc-04.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="info-name">
                            <a href="#">Real Mandrid FC</a>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2">
                    <div class="event-home-item">
                        <div class="retangle">
                            <div class="inner">
                                <a href="#">
                                    <img src="img/event-item/fc-05.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="info-name">
                            <a href="#">Real Mandrid FC</a>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-2">
                    <div class="event-home-item">
                        <div class="retangle">
                            <div class="inner">
                                <a href="#">
                                    <img src="img/event-item/fc-06.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="info-name">
                            <a href="#">Real Mandrid FC</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="uk-grid wrap-showmore">
                <div class="uk-width-1-1">
                    <div class="uk-text-center">
                        <a id="show-more" href="#" class="show-more"><b>show 10 more popular teams…</b></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<?php include "include/offcanvas-menu.php" ?>
<?php include "include/footer.php" ?>

<script>
    $(document).ready(function(){
        var item = '<div class="uk-width-1-2">';
            item += '<div class="event-home-item">';
            item += '<div class="retangle">';
            item += '<div class="inner">';
            item += '<a href="#">';
            item += '<img src="img/event-item/fc-01.png" alt="">';
            item += '</a>';
            item += '</div>';
            item += '</div>';
            item += '<div class="info-name">';
            item += '<a href="#">Real Mandrid FC</a>';
            item += '</div>';
            item += '</div>';
            item += '</div>';

        $("#show-more").on('click', function(e){
            e.preventDefault();
            for (var i = 1; i <= 10; i++)
            {
                $("#wrap-list-event").append(item);
            }
        })
    });
</script>

